Ext.define('Datamatic.store.TreeData', {
    extend: 'Ext.data.TreeStore',
    requires: 'Datamatic.model.TreeDataModel',
    model: 'Datamatic.model.TreeDataModel',
    // Called from a node's removeChild & removeAll methods.
    beforeNodeRemove: function(parentNode, childNodes) {
    	if (!Ext.isArray(childNodes)) {
		    childNodes = [ childNodes ];
		}

		this.fireEvent("beforeremove", this, childNodes);
    	this.callParent(arguments);
    },
    getRootData: function(){
    	return this.getNodeData(this.root.data);
    },
    hasData: function(){
        return this._hasData;
    },
    getNodeData: function(nodeData){
        var i = 0;
        var fieldName = Handsontable.helper.spreadsheetColumnLabel(i);

        var result = {
    		expanded:nodeData.expanded || !!nodeData.children,
    		leaf:!nodeData.children
    	}

        while (typeof nodeData[fieldName] != "undefined"){
            result[fieldName] = nodeData[fieldName];
            i++;
            fieldName = Handsontable.helper.spreadsheetColumnLabel(i);
        }

        //backwards compatibility
        result.name = typeof result["A"] !="undefined"?result["A"]:nodeData.name;
        result.size = typeof result["B"] !="undefined"?result["B"]:nodeData.size;

    	if (nodeData.children){
    		result.children = [];
    		Ext.iterate(nodeData.children, function(child){
    			result.children.push(this.getNodeData(child, false));
    		}.bind(this));
    	}

    	return result;
    },
    resetTreeData: function(){
        this.setRootNode(this.getRootData());
    },
    constructor: function(){
        Ext.data.TreeStore.superclass.constructor.apply(this, arguments);

        // FIXME: this is a hack to prevent issue in the TreeData editor
        // which occures sporadically, need to find out what's going on
        // ond only reset tree data on certain messages.
        // Ext.get(window).on("error", function(ev){
        //     console.log(ev);
            //this.resetTreeData();
        // }, this);

        this.on("datachanged", function(){
            this._hasData = true;
            this.fireEvent("change");
        }, this);

        this.on("update", function(store, record, operation, modifiedFieldNames){
            this._hasData = true;
            this.fireEvent("change");
        }, this);
    },
    root:{
		expanded: true,
		children:[]
    }
});
