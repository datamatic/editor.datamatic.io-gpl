/**
 * DocumentManager manages opening, creating, closing or removing documentClosed
 * TODO: complete this
 */
Ext.define('Datamatic.document.DocumentManager', {
    extend: 'Ext.Component',
    constructor: function () {
        this.callParent();
        this._currentDocument = null;
        this._newDocumentData = null;
    },
    /**
     * Creates a new document with sample data
     * @param {NewDocumentData}
     *
     */
    newDocument: function(newDocumentData){
        this.closeCurrentDocument();
        Datamatic.drive.createNewFileAndRedirect(newDocumentData);
    },
    /**
     * Closes current document
     */
    closeCurrentDocument: function(){
        Datamatic.drive.closeDocument();
    }
});
