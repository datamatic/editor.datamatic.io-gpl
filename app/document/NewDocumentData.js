Ext.define('Datamatic.document.NewDocumentData', {
    constructor: function (newDocumentData) {
        this._newDocumentData = newDocumentData;
    },
    getData: function(){
        return this._newDocumentData.data;
    },
    getSettings: function(){
        return this._newDocumentData.settings;
    },
    getTemplateID: function(){
        return this._newDocumentData.templateID;
    },
    getCode: function(){
        return this._newDocumentData.code;
    }
});
