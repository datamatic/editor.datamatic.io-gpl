Ext.define('Datamatic.view.editor.output.SandboxedOutput', {
    extend: 'Ext.Component',
    xtype: 'sandboxed-output',
    itemId: "editor-output",
    cls:'datamatic-output',
    initComponent: function () {
        this.callParent();

        // create settings object, set waterrmark url default property
        this._settings = {
          "watermark-url":Ext.manifest.vars.WATERMARK_URL
        };

        // lookup & bind store
        this.store = Ext.StoreMgr.lookup(this.store);
        this.bindStore(this.store);

        setTimeout(function(){
            this.relayEvents(Ext.ComponentQuery.query('#properties-view')[0], ["change"]);
        }.bind(this));

        this.on("resize", this._updateSandboxPosition, this);

        this._updateTask = new Ext.util.DelayedTask(function(action){
          this._postMessage({
            action:action || "update",
            template:this._templateID,
            settings:this._settings,
            data:this.store.getRootData()
          }, "*");
        }, this);
    },
    store: 'TreeData',
    bindStore: function (store) {
        this.mon(store, {
            refresh: this.onStoreRefresh,
            update: this.onStoreUpdate,
            scope: this
        });

        this.mon(store, {
            add: this.onStoreAdd,
            beforeremove: this.onBeforeStoreRemove,
            scope: this
        });
    },

    // happens when loaded
    onStoreRefresh: function (store) {
        this.loadData(store.getRootData());
    },

    onBeforeStoreRemove: function (store, records) {
        Ext.iterate(records, function (record) {
            var parentDataNode = this._getDataNodeFromRecord(record.parentNode);

            if (parentDataNode) {
                var parentDataChildren = parentDataNode.children || parentDataNode._children;
                // need to use index from data as parentNode.indexOf(record) doesn't work anymore
                // as beforeremove is called after the node has been removed
                // see http://www.sencha.com/forum/showthread.php?285387-beforeremove-event-not-fired-on-TreeStore-when-removing-child-nodes
                var index = record.parentNode.data.children.indexOf(record.data)
                if (parentDataChildren) {
                    parentDataChildren.splice(index, 1);
                    this._update();
                }
            }
        }.bind(this));
    },

    // happens when new node is added
    // TODO: note that this also happens when tree in the panel is expanded, try to prevent this
    onStoreAdd: function (store, records, index) {
        Ext.iterate(records, function (record) {
            var parentDataNode = this._getDataNodeFromRecord(record.parentNode);

            if (parentDataNode) {
                var parentDataChildren = parentDataNode.children || parentDataNode._children;

                if (!parentDataChildren) {
                    parentDataNode.children = parentDataChildren = [];
                }

                if (!parentDataChildren[index - 1]) {
                    parentDataChildren.push({
                        A: record.get("A")
                    });
                }

                this._update();
            }
        }.bind(this));
    },

    // happens when node is updated
    onStoreUpdate: function (store, record, operation, modifiedFieldNames) {
        var dataNode = this._getDataNodeFromRecord(record);
        if (modifiedFieldNames){
            modifiedFieldNames.forEach(function(fieldName){
                if (dataNode && dataNode[fieldName] != record.get(fieldName)) {
                    dataNode[fieldName] = record.get(fieldName);
                    this._update();
                }
            }.bind(this));
        }
    },

    // retrieves data node from record
    _getDataNodeFromRecord: function (record) {
        var selectedIndexes = [];
        var storeNode = record;
        var dataNode = this._root;
        if (!dataNode) return;

        // getting indexes in reverse order
        while (storeNode.parentNode) {
            selectedIndexes.unshift(storeNode.parentNode.indexOf(storeNode));
            storeNode = storeNode.parentNode;
        }

        // getting data node based on indexes
        Ext.iterate(selectedIndexes, function (index) {
            var children = dataNode.children || dataNode._children;
            if (children) {
                dataNode = children[index];
            }
        }.bind(this));

        return dataNode;
    },

    getTemplateID: function(){
      return this._templateID;
    },

    setTemplate: function (templateID, preventLoad) {
        this._clearSandbox();
        return new Promise(function(success, failure){
          this._templateID = templateID;
          this._settings = {};
          Ext.ComponentQuery.query('#properties-view')[0].clearProperties();
          this._renderTemplate(templateID, function(){
            if (!preventLoad){
                this.loadData(this.store.getRootData());
            }

            this.fireEvent("templateChange", templateID);
            success();
          }.bind(this));
        }.bind(this));
    },
    setCode: function(code){
        delete this._properties;

      this._postMessage({
        action:"setCode",
        data:this._data,
        code:code
      });

      this._code = code;

      Ext.ComponentQuery.query('#properties-view')[0].clearProperties();
      return this.initSettings().then(function(){
          this.fireEvent("codeChange", code);
      }.bind(this));
    },

    getCode: function(){
      return this._code || '';
    },

    _postMessage: function(data){
      this._sandboxEl.dom.contentWindow.postMessage(data, "*");
    },

    download: function(type){
      this._sandboxEl.dom.setAttribute("sandbox", "allow-scripts allow-popups allow-same-origin");

      this._postMessage({
        action:"download",
        fileTitle:Datamatic.drive.getFileTitle(),
        type:type
      });
    },
    _getSandboxURL: function(templateID){
        return Ext.manifest.vars.RUNTIME[templateID].SANDBOX_URL;
    },
    _loadSandbox: function(templateID, callback){
      var sandBoxUrl = this._getSandboxURL(templateID);

      this._sandboxEl.on("load", callback, this, {
        single:true
      })
      this._sandboxEl.dom.src = sandBoxUrl;
    },
    _clearSandbox: function(){
        this._sandboxEl.dom.src = "about:blank";
    },
    _renderTemplate: function (templateID, callback) {
      this._loadSandbox(templateID, function(){
        this._update("render");
        this.on("sandboxRendered", function(){
            this._postMessage({
              action:"enableEditing"
            });
            callback();
        }, this, {
            single:true
        });
      }.bind(this));
    },
    _isTreeDataType: function(prop){
        return prop.property.indexOf("-type")!=-1 && prop.noPropertyEditor && prop.value == "Tree";
    },
    _handleDataSources: function(templateProperties){
        var treeDataTypeProperty = templateProperties.filter(this._isTreeDataType)[0];
        var breadcrumb = Ext.ComponentQuery.query('#data-editor-breadcrumb')[0];

        if(treeDataTypeProperty){
            breadcrumb.show();

            var maxLevelPropName = treeDataTypeProperty.property.replace("-type", "-max-levels")
            var treeDataLevelProperty = templateProperties.filter(function(prop){
                return prop.property == maxLevelPropName;
            })[0];

            if (treeDataLevelProperty){
                breadcrumb.setLevel(treeDataLevelProperty.defaultValue);
            } else {
                breadcrumb.setLevel(null);
            }
        } else {
            breadcrumb.hide();
        }
    },
    initSettings: function (resetDefaults) {
        var settings = this._settings;
        var properties = [];

        return new Promise(function(success, failure){
            this._requestProperties().then(function(templateProperties){
                Ext.ComponentQuery.query('#properties-view')[0].clearProperties();

                this._handleDataSources(templateProperties);

                var restrictedProperties = Datamatic.app.getController("UserPlanRestrictionsController").getProperties();
                var allProperties = templateProperties.concat(restrictedProperties);

                var propertiesMap = {};

                Ext.iterate(allProperties, function (prop) {
                    if (typeof settings[prop.property] == "undefined" || (resetDefaults && !prop.noPropertyEditor)){
                        settings[prop.property] = prop.defaultValue;
                    }

                    if (prop.noPropertyEditor && prop.value){
                        settings[prop.property] = prop.value;
                    }

                    // dat.gui needs reference to the settings
                    prop.settings = settings;
                    // adding property to the clear

                    properties.push(prop);
                    propertiesMap[prop.property] = prop;
                    prop.onChange = function (value) {
                        if (!this._localPropertyChange){
                          this._update();
                        }
                    }.bind(this);
                }, this);

                // cleanup settings that are not part of the template
                Ext.iterate(this._settings, function (prop) {
                    if (!propertiesMap[prop]) delete this._settings[prop];
                }, this);

                Ext.ComponentQuery.query('#properties-view')[0].loadProperties(properties, this._settings);

                success();
            }.bind(this));
        }.bind(this));
    },

    _requestProperties: function(){
        return new Promise(function(success, failure){
            // return cached if available
            if (this._properties) success(this._properties);

            this._postMessage({
              action:"requestProperties"
            });

            this.on("returnProperties", function(properties){
                this._properties = properties;
                success(properties);
            }, this, {
                single:true
            });
        }.bind(this));
    },

    loadData: function (root) {
        this._root = root;
        this._data = root.children[0];
        this._update();
    },
    getSettings: function () {
        return this._settings;
    },
    resetDefaults: function(){
      Ext.ComponentQuery.query('#properties-view')[0].clearProperties();
      this.initSettings(true).then(function(){
          this._update();
          this.fireEvent("change");
      }.bind(this));
    },
    setSettings: function(settings){
      this.loadSettings(settings);
      this.fireEvent("change");
    },
    refreshSettings: function(){
      Ext.ComponentQuery.query('#properties-view')[0].clearProperties();
      this.initSettings();
    },
    loadSettings: function (settings) {
        Ext.iterate(settings, function(key, value){
            this._settings[key] = value;
        }, this);

        this.refreshSettings();
        this._update();
    },
    _update: function (action) {
        if (action == "render"){
          this._postMessage({
            action:action || "update",
            editing:"true",
            template:this._templateID,
            settings:this._settings,
            data:this.store.getRootData()
          }, "*");
        } else {
          this._updateTask.delay(5, null, null, [action]);
        }
    },
    _onSandboxMessage: function(event){
      var data = event.data;
      switch (data.action) {
        case "changePosition": return this._onSandboxChangePosition(data.settings);
        case "changeProperty": return this._onSandboxChangeProperty(data);
        case "changeProperties": return this._onSandboxChangeProperties(data.properties);
        case "returnProperties": return this._onSandboxReturnProperties(data.properties);
        case "rendered": return this._onSandboxRendered();
      }
    },
    _onSandboxRendered: function(){
        this.fireEvent("sandboxRendered");
        this._updateSandboxPosition();
    },
    _onSandboxReturnProperties: function(properties){
        this.fireEvent("returnProperties", properties);
    },
    _onSandboxChangeProperty: function(data){
      Ext.ComponentQuery.query('#properties-view')[0].setProperty(data.propertyName, data.propertyValue);
    },

    _onSandboxChangeProperties: function(properties){
      Ext.iterate(properties, function(prop, value){
        this._localPropertyChange = true;
        Ext.ComponentQuery.query('#properties-view')[0].setProperty(prop, value);
        this._localPropertyChange = false;
      }, this);

    },
    _onSandboxChangePosition: function(settings){
        Ext.iterate(settings, function(key, value){
          this._settings[key] = value;
        }, this);
        this.fireEvent("change");
    },
    // create iframe mask and show / hide when dragging all splitters
    _handleSplittersResize: function(){
      this._splitterMask = Ext.get(document.body).createChild({
          cls:"datamatic-sandbox-mask"
      });

      var bindTrackerEvents = function(tracker){
          tracker.on("beforedragstart", this._onBeforeSplitterResize, this);
          tracker.on("dragend", this._onAfterSplitterResize, this);
          tracker.on("mouseup", this._onAfterSplitterResize, this);
      }.bind(this);

      Ext.ComponentQuery.query('splitter').forEach(function(splitter){
          if (splitter.tracker){
              bindTrackerEvents(splitter.tracker);
          } else {
              splitter.on("render", bindTrackerEvents, this, {
                  single:true
              });
          }
      });

      // need to bind document mouseup as well as in some cases, we don't get mouseup on tracker
      Ext.get(document).on("mouseup", this._onAfterSplitterResize, this);
    },
    _onBeforeSplitterResize: function(){
        this._splitterMask.show();
        this._splitterMask.setBox(this.el.getBox());
    },
    _onAfterSplitterResize: function(){
        this._splitterMask.hide();
    },
    _updateSandboxPosition: function(){
        if (this._sandboxEl){
            this._sandboxEl.setBox(this.el.getBox());
        }
    },
    afterRender: function () {
      this._sandboxEl = Ext.get(document.body).createChild({
        tag:"iframe",
        // sandbox:"allow-scripts allow-popups",
        //sandbox:"allow-scripts allow-popups",
        style:"position:fixed",
        cls:"datamatic-template-sandbox",
        src:"about:blank"
      });

      window.addEventListener("message", this._onSandboxMessage.bind(this), false);
      this._handleSplittersResize();
    }
});
