Ext.define('Datamatic.view.widgets.DataEditorColorPicker', {
    extend: 'Ext.Base',
    destroy: function(){
        this.callParent();
        $(this._element).spectrum("destroy");
    },
    constructor: function(config){
      this.callParent();

      if (!config.element) return;

      this._element = config.element;

      $(config.element).spectrum({
          color: config.value,
          showPalette: true,
          showPaletteOnly: false,
          togglePaletteOnly: true,
          preferredFormat: "hex",
          maxSelectionSize:8,
          cancelText: "Cancel",
          showInput: true,
          chooseText: "OK",
          beforeShow: function(){
              var isEditing = ($(".handsontableInput").is(":visible"));
              return isEditing;
          },
          beforeHide: function(){
              var isEditing = ($(".handsontableInput").is(":visible"));
              return !isEditing;
          },
          showAlpha: true,
          togglePaletteMoreText: 'All Colors...',
          togglePaletteLessText: 'All Colors...',
          localStorageKey: false,
          clickoutFiresChange: true,
          palette: Datamatic.view.widgets.DataEditorColorPicker.MATERIAL_PALETTE,
          showSelectionPalette: false,
          move: config.onChange,
          change: config.onChange
      });
    }
});

// material palette from https://material.google.com/style/color.html#color-color-palette
Datamatic.view.widgets.DataEditorColorPicker.MATERIAL_PALETTE = [
    /* grey */  ["#000000",  "#212121", "#424242", "#616161", "#757575", "#9E9E9E", "#BDBDBD", "#E0E0E0", "#EEEEEE", "#F5F5F5", "#FAFAFA", "#FFFFFF"],

                /*RED*/    /*PINK*/   /*PURPLE*/ /*INDIGO*/ /*BLUE*/   /*CYAN*/   /*GREEN*/  /*LIME*/   /*YELLOW*/ /*ORANGE*/ /*BROWN*/  /*BLUE GREY*/
    /* 500 */  ["#F44336", "#E91E63", "#9C27B0", "#3F51B5", "#2196F3", "#00BCD4", "#4CAF50", "#CDDC39", "#FFEB3B", "#FF5722", ,"#795548", "#607D8B"],
    /* 50 */   ["#FFEBEE", "#FCE4EC", "#F3E5F5", "#E8EAF6", "#E3F2FD", "#E0F7FA", "#E8F5E9", "#F9FBE7", "#FFFDE7", "#FBE9E7", ,"#EFEBE9", "#ECEFF1"],
    /* 100 */  ["#FFCDD2", "#F8BBD0", "#E1BEE7", "#C5CAE9", "#BBDEFB", "#B2EBF2", "#C8E6C9", "#F0F4C3", "#FFF9C4", "#FFCCBC", ,"#D7CCC8", "#CFD8DC"],
    /* 200 */  ["#EF9A9A", "#F48FB1", "#CE93D8", "#9FA8DA", "#90CAF9", "#80DEEA", "#A5D6A7", "#E6EE9C", "#FFF59D", "#FFAB91", ,"#BCAAA4", "#B0BEC5"],
    /* 300 */  ["#E57373", "#F06292", "#BA68C8", "#7986CB", "#64B5F6", "#4DD0E1", "#81C784", "#DCE775", "#FFF176", "#FF8A65", ,"#A1887F", "#90A4AE"],
    /* 400 */  ["#EF5350", "#EC407A", "#AB47BC", "#5C6BC0", "#42A5F5", "#26C6DA", "#66BB6A", "#D4E157", "#FFEE58", "#FF7043", ,"#8D6E63", "#78909C"],
    /* 500 */  ["#F44336", "#E91E63", "#9C27B0", "#3F51B5", "#2196F3", "#00BCD4", "#4CAF50", "#CDDC39", "#FFEB3B", "#FF5722", ,"#795548", "#607D8B"],
    /* 600 */  ["#E53935", "#D81B60", "#8E24AA", "#3949AB", "#1E88E5", "#00ACC1", "#43A047", "#C0CA33", "#FDD835", "#F4511E", ,"#6D4C41", "#546E7A"],
    /* 700 */  ["#D32F2F", "#C2185B", "#7B1FA2", "#303F9F", "#1976D2", "#0097A7", "#388E3C", "#AFB42B", "#FBC02D", "#E64A19", ,"#5D4037", "#455A64"],
    /* 800 */  ["#C62828", "#AD1457", "#6A1B9A", "#283593", "#1565C0", "#00838F", "#2E7D32", "#9E9D24", "#F9A825", "#D84315", ,"#4E342E", "#37474F"],
    /* 900 */  ["#B71C1C", "#880E4F", "#4A148C", "#1A237E", "#0D47A1", "#006064", "#1B5E20", "#827717", "#F57F17", "#BF360C", ,"#3E2723", "#263238"]
];