Ext.define('Datamatic.view.tools.Code', {
    extend: 'Ext.panel.Panel',
    xtype: 'code',
    itemId:"code-view",
    cls:"x-panel-default-border datamatic-tool-code",
    initComponent: function () {
        this._data = [];
        this.callParent();

        this.on("resize", this._onPanelResize, this);

        $(window).keydown(function(event) {
            if (event.which == 83 && event.ctrlKey) {
              if (this._editor){
                this.fireEvent("save", this._editor.getValue());
              }

              event.preventDefault();
              return false;
          }
        }.bind(this));
    },
    _onPanelResize: function(panel, width, height){
      if (this._editor) {
        this._editor.resize();
      }
    },
    setCode: function(code){
      if (!this._editor){
          this.body.dom.innerHTML = "";

        var ct = this.body.createChild({
          cls:"datamatic-code-editor-ct"
        });
        var editor = this._editor = ace.edit(ct.dom);
        editor.setTheme("ace/theme/chrome");
        ace.require("ace/ext/language_tools");
        editor.getSession().setMode("ace/mode/javascript");
        editor.setValue(code, -1);

        // enable content assist
        editor.setOptions({
            enableBasicAutocompletion: true
        });

        editor.commands.removeCommand({})
        editor.commands.addCommand({
            name: 'save',
            bindKey: {win: 'Ctrl-S',  mac: 'Command-S'},
            exec: function(){
              this.fireEvent("save", editor.getValue());
            }.bind(this)
        });

        editor.getSession().on('change', function(){
          this.fireEvent("change", editor.getValue());
        }.bind(this));
      } else {
        this._editor.session.getDocument().setValue(code);
      }
    },
    getCode: function(){
      if (this._editor){
        return this._editor.getValue();
      } else return false;
    }
});
