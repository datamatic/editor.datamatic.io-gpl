Ext.define('Datamatic.view.tools.Result', {
    extend: 'Ext.panel.Panel',
    xtype: 'result',
    itemId:"result-view",
    id:'datamatic-tool-result',
    cls:"x-panel-default-border datamatic-tool-result",
    alias: 'tools.Result',
    isValidParent: function(){
        return true;
    },
    items: [{
    	xtype:"sandboxed-output"
    }]
});
