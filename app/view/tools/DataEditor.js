Ext.define('Datamatic.view.tools.DataEditor', {
    extend: 'Ext.panel.Panel',
    xtype: 'dataEditor',
    itemId: "data-editor",
    title:"Data Editor",
    requires: 'Ext.form.field.Number',
    cls: "x-panel-default-border x-panel-data-editor",
    requires: ['Datamatic.view.widgets.DataEditorColorPicker'],
    minSpareRows: 1,
    minSpareCols: 1,
    header:false,
    initComponent: function () {
        this._data = [];
        this.callParent();

        this.on("resize", this._onPanelResize, this);

        this._renderersRegistry = [];
    },

    _onPanelResize: function(){
        if (this._editor) {
            this._editor.render();
        }
    },
    updateSettings: function (settings) {
        if (this._editor) {
            this._editor.updateSettings(settings);
        }
    },
    isUndoAvailable: function () {
        if (this._editor) return this._editor.isUndoAvailable();
    },
    isRedoAvailable: function () {
        if (this._editor) return this._editor.isRedoAvailable();
    },
    undo: function () {
        if (this._editor) this._editor.undo();
    },
    redo: function () {
        if (this._editor) this._editor.redo();
    },
    copy: function(){
        this._editor.copyPaste.setCopyableText();
        this.fireEvent("copy");
    },
    paste: function(){
        this._editor.copyPaste.triggerPaste();
        this.fireEvent("paste");
    },
    cut: function(){
        this._editor.copyPaste.triggerCut();
        this.fireEvent("cut");
    },
    remove: function(){
      var range = this._editor.getSelectedRange();
      this.fireEvent("removeSelectedRows", range);
    },
    _isColorCell: function(){
        var rgbRegex = /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/;

    },
    _cellRenderer: function(hotInstance, TD, row, col, prop, value, cellProperties){

        Ext.get(TD).on("dblclick", function(e){
            e.stopEvent();
        });

        // if (value === "true" || value ==="false" || value === false || value === true) return Handsontable.renderers.getRenderer('checkbox').apply(this, arguments);

        // disable custom renderers when number of rows is higher than limit
        if (this._data && this._data.length>this.VIRTUAL_RENDER_LIMIT) {
            TD.innerHTML = value;
            return
        }
        
        var isColor = String(value).match(/(#[\d\w]+|\w+\((?:\d+%?(?:,\s)*){3}(?:\d*\.?\d+)?\))/i);
        if (isColor){
            var color = isColor?tinycolor(value):undefined;
            TD.style.backgroundColor = value;
            TD.style.color = color.getBrightness()>150?"black":"white";
            if (!cellProperties.colorPickerEditor){
                var colorPickerEditor = new Datamatic.view.widgets.DataEditorColorPicker({
                    element:TD,
                    cellProperties:cellProperties,
                    value:value,
                    onChange:function(row, col, TD, color){
                        this._editor.setDataAtCell(row, col, color.toString());
                    }.bind(this, row, col, TD)
                });

                cellProperties.colorPickerEditor = colorPickerEditor;
                this._renderersRegistry.push(colorPickerEditor);
            }
        } else if (cellProperties.colorPickerEditor){
            cellProperties.colorPickerEditor.destroy();
            delete cellProperties.colorPickerEditor;
        }

        TD.innerHTML = value;
    },
    _cellEditor: function(){
        return Handsontable.editors.getEditor('checkbox').apply(this, arguments);
    },    
    _destroyRenderers: function(){
        this._renderersRegistry.forEach(function(renderer){
            renderer.destroy();
        });

        this._renderersRegistry.length = 0;
    },
    VIRTUAL_RENDER_LIMIT: 500,
    setData: function (data, forceSelection) {
        this._data.length = 0;
        this._data.push.apply(this._data, data);
        var me = this;
        var renderingOffset = data.length>this.VIRTUAL_RENDER_LIMIT?Infinity:undefined;

        this._destroyRenderers();

        if (!this._editor) {
            var ct = this.body.dom;
            var minRows = this.body.getHeight() / 25;
            this._editor = $(ct).handsontable({
                data: this._data,
                outsideClickDeselects: false,
                manualColumnResize: true,
                undo: false,
                // renderer as custom function
                renderer: this._cellRenderer.bind(this),
                // editor: this._cellEditor.bind(this),
                minSpareRows: this.minSpareRows,
                minSpareCols:this.minSpareCols,
                colWidths:125,
                viewportColumnRenderingOffset:renderingOffset,
                viewportRowRenderingOffset:renderingOffset,
                afterChange: this._afterEditorChange.bind(this),
                afterCreateRow: this._afterCreateRow.bind(this),
                afterRemoveRow: this._afterRemoveRow.bind(this),
                afterSelection: this._afterSelection.bind(this),
                beforeKeyDown: this._beforeKeyDown.bind(this),
                colHeaders:true,
                rowHeaders:true,
                contextMenu: ["row_above", "row_below", "remove_row"]
            }).handsontable('getInstance');
        } else {
            this._editor.loadData(this._data);
        }
    },
    getData: function(){
        return this._data;
    },
    setDataAtCell: function(row, cell, value){
        this._editor.setDataAtCell(row, cell, value, "local");
    },
    deselect: function () {
        if (this._editor) {
            this._editor.deselectCell();
        }
    },
    selectRow: function (selectedIndex) {
        if (this._editor) {
            this._editor.selectCell(selectedIndex, 0);
        }
    },
    _afterRemoveRow: function (rowIndex, amount) {
        this.fireEvent("afterRemoveRow", rowIndex, amount);
    },
    _afterCreateRow: function (rowIndex, amount) {
        this.fireEvent("afterCreateRow", rowIndex, amount);
    },
    _afterEditorChange: function (changes, type) {
      if (!changes) return;
      for (var i = 0; i < changes.length; i++) {
        var change = changes[i];
        if (type != "local"){
            this.fireEvent("afterEditorChange", change, type);
        }
      }
    },
    _afterSelection: function (rowIndexStart, cellIndexStart, rowEndIndex, cellEndIndex) {
        this.fireEvent("afterSelection", rowIndexStart, cellIndexStart, rowEndIndex, cellEndIndex);
    },
    _stopEvent: function(ev){
      ev.isImmediatePropagationEnabled = false;
      ev.isImmediatePropagationStopped = function(){
        return true;
      };
    },
    _isDeleteKey: function(ev){
      return (ev.metaKey && ev.keyCode == 8) || ev.keyCode == 46;
    },
    _isEditing: function(){
        return ($(".handsontableInput").is(":visible"));
    },
    _isCopyKey: function(ev){
        return ev.ctrlKey && ev.keyCode == 67;
    },
    _isPasteKey: function(ev){
        return ev.ctrlKey && ev.keyCode == 86;
    },
    _isCutKey: function(ev){
        return ev.ctrlKey && ev.keyCode == 89;
    },
    _allowRowDelete: function(){
        return this._editor.getSelectedRange().getAll().every(function(cell){
            // return this._editor.getDataAtCell(cell.row, cell.col) === "";
            // allow delete when first column is empty
            return cell.col === 0 && this._editor.getDataAtCell(cell.row, cell.col) === "";
        }.bind(this));
    },
    _handleEmptyRow: function(ev){
        if (this._isDeleteKey(ev) && this._allowRowDelete() && !this._isEditing()) {
            this._stopEvent(ev);
            this.remove();
        }
    },
    _beforeKeyDown: function (ev) {
        if (Ext.WindowManager.getActive()) return false;
        if (ev.keyCode == 37 && ev.ctrlKey && ev.shiftKey) {
            this._stopEvent(ev);
            this.fireEvent("navigateUp");
        }
        if (ev.keyCode == 39 && ev.ctrlKey && ev.shiftKey) {
            this._stopEvent(ev);
            this.fireEvent("navigateDown");
        }

        if (this._isCopyKey(ev)){
            this.fireEvent("copy");
        }

        if (this._isPasteKey(ev)){
            this.fireEvent("paste", ev.shiftKey);
        }

        if (this._isCutKey(ev)){
            this.fireEvent("cut");
        }

        // handle delete on empty row
        // note that this intentionally works on the second key down
        // when row is already empty (before change)
        try {
            this._handleEmptyRow(ev);
        } catch (e){
            // do nothing, throws Cannot set property 'forceFullRender' of null, TODO: investigate why
        }
    },
    removeRows: function(row, amount){
        this._editor.alter('remove_row', row, amount);
    },
    getSelectedData: function () {
        var selection = this._editor.getSelected();
        return this._editor.getData.apply(this._editor, selection);
    },
    alias: 'tools.DataEditor',
    split: true
});
