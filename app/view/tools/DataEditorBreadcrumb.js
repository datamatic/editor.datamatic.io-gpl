Ext.define('Datamatic.view.tools.DataEditorBreadcrumb', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'data-editor-breadcrumb',
    itemId: 'data-editor-breadcrumb',
    items: [],
    update: function(selectedNode, inactiveNode) {
        this._updateTask.delay(5, null, null, [selectedNode, inactiveNode]);
        this._selectedNode = selectedNode;
    },
    EMPTY_BTN_TEXT:"(empty)",
    _getButtonText: function(text) {
        if (text === "") return this.EMPTY_BTN_TEXT;
        return text;
    },
    _updateTree: function(selectedNode, inactiveNode) {
        var me = this;
        var getNodeLevel = function(node) {
            var level = -1;
            var n = node;
            while (n.parentNode) {
                level++;
                n = n.parentNode;
            }

            return level;
        }

        var supportsAnotherLevel = function(selectedNode){
            var selectedNodeLevel = getNodeLevel(selectedNode);
            return !this._level || this._level > selectedNodeLevel;
        }.bind(this)

        if (selectedNode) {
            this.removeAll();
            var parentNode = selectedNode.parentNode;
            var parents = [];
            while (parentNode && parentNode.parentNode && parentNode.parentNode.parentNode) {
                parents.push({
                    text: this._getButtonText(parentNode.data.A),
                    minWidth: 35,
                    node: parentNode,
                    handler: function() {
                        me.fireEvent("navigateTo", this.node);
                    }
                });
                parentNode = parentNode.parentNode;
            }

            for (var i = parents.length - 1; i > -1; i--) {
                this.add(parents[i], "-");
            }

            this._selectedBtn = this.add({
                pressed: true,
                minWidth: 35,
                node: selectedNode,
                handler: function() {
                    me.fireEvent("navigateTo", this.node);
                },
                text: this._getButtonText(selectedNode.data.A)
            });

            if (selectedNode.firstChild) {
                this._nextBtn = this.add(">", {
                    handler: function() {
                        me.fireEvent("navigateDown");
                    },
                    text: "Open"
                })[1];
            } else if (supportsAnotherLevel(selectedNode)) {
                this._nextBtn = this.add(">", {
                    text: "New..",
                    handler: function() {
                        me.fireEvent("navigateDown");
                    }
                })[1];
            }
        } else if (this._selectedBtn) {
            this._selectedBtn.setText(this.EMPTY_BTN_TEXT);

            if (inactiveNode) {
                this._selectedBtn.setText(this._getButtonText(inactiveNode.data.A));
                this._selectedBtn.handler = function() {
                    me.fireEvent("navigateTo", inactiveNode);
                }

                if (supportsAnotherLevel(inactiveNode)){
                    this._nextBtn.disable();
                    this._nextBtn.setText("New..");
                }
            } else {
                this._nextBtn.hide();
            }
        }

        var lastItem = this.items.last();
        if (lastItem && lastItem.el) {
            this.items.last().el.scrollIntoView(this.el.query(".x-box-scroller-body-horizontal")[0]);
        }
    },
    enableOverflow: true,
    overflowHandler: "scroller",
    cls: "data-editor-breadcrumb",
    setLevel: function(level) {
        this._level = level;

        if(this._selectedNode){
            this.update(this._selectedNode);
        }
    },
    initComponent: function() {
        Ext.toolbar.Toolbar.shortcuts["<"] = {
            xtype: 'tbseparator',
            cls: 'toolbar-separator-left'
        };
        Ext.toolbar.Toolbar.shortcuts[">"] = {
            xtype: 'tbseparator',
            cls: 'toolbar-separator-right'
        };

        this._updateTask = new Ext.util.DelayedTask(function(selectedNode, inactiveNode) {
            this._updateTree(selectedNode, inactiveNode);
        }, this);

        this.callParent();
    }
});