Ext.define('Datamatic.view.tools.DataTree', {
    extend: 'Ext.tree.Panel',
    xtype: 'dataTree',
    title:"Data Tree",
    cls:"data-tree x-panel-default-border",
    itemId:"data-tree",
    split: true,
    animate:false,
    useArrows: true,
    hideHeaders:true,
    initComponent: function(){
    	this.callParent();

    	this.store.on("refresh", function(){
            this.getSelectionModel().selectRange(1,1);
    	}, this, {
    		buffer:10,
            single:true
    	});
    },
    selectByReferenceIndex: function(indexes){
        var node = this.store.root;
        for (var i = indexes.length; i--; ) {
            if (!node) break;
            node = node.getChildAt(indexes[i]);
        }

        if (node){
            this.getSelectionModel().select(node);
        } else {
            this.getSelectionModel().selectRange(1,1);
        }
    },
    rootVisible: false,
    store:'TreeData',
    columns: [
    // {
    //     xtype: 'treecolumn',
    //     text: 'Name',
    //     width: "100%",
    //     sortable: true,
    //     dataIndex: 'name'
    // }
    ]
});
