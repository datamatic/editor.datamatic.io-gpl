Ext.define('Datamatic.view.tools.Properties', {
    extend: 'Ext.panel.Panel',
    xtype: 'properties',
    itemId: "properties-view",
    alias: 'tools.Properties',
    closable: true,
    collapsible:false,
    closeAction: "hide",
    title: 'Properties',
    cls: "datamatic-tool-properties datamatic-tool-dark-header properties-loading",
    initComponent: function () {
        this.on("render", this.__onRender, this);
        this.callParent();
        this.on("beforecollapse", function(){
          return false;
      }, this);
      this._onChangeTask = new Ext.util.DelayedTask(function(item, value){
          if (item.oldValue === value) return;

          item.onChange(value);
          this.fireEvent("change");
          item.oldValue = value;
      }, this);
    },
    // moving theme property to the first position
    _handleThemePropertyOrder: function(cfg){
        var themeItem = cfg.filter(function(item){
            return item.property == "theme";
        })[0];

        if (themeItem) {
            // move item from current position to 0
            cfg.splice(0, 0, cfg.splice(cfg.indexOf(themeItem), 1)[0]);
        }

        return cfg;
    },
    loadProperties: function (cfg, settings) {
        var _this = this;
        if (!this.rendered) {
            this.on("render", function () {
                this.loadProperties(cfg, settings);
            }, this, {
                single: true,
                buffer: 10
            });
            return;
        }

        cfg = this._handleThemePropertyOrder(cfg);

        this._controllers = [];
        this._propertiesMap = {};
        this._noEditorPropertiesMap = {};
        this._settings = settings;

        Ext.iterate(cfg, function (item) {
            var controller;
            var folder = this._gui;

            if (item.noPropertyEditor) {
              return this._noEditorPropertiesMap[item.property] = item;
            }

            item.oldValue = settings[item.property];

            if (item.folder) {
                try {
                    folder = this._gui.addFolder(item.folder);
                    Ext.get(folder.domElement).select(".title", true).on("click", this.onFolderClick, {
                      folder:folder,
                      folderName:item.folder
                    });
                } catch (e) {
                    folder = this._gui.__folders[item.folder];
                }
            }

            if (item.type == "color") {
                controller = folder.addColor(item.settings, item.property);
                controller.domElement.className = "color-input";
                this.appendSpectrumController(controller, settings);
            } else if (item.values) {
                controller = folder.add(item.settings, item.property, item.values);
            } else {
                controller = folder.add(item.settings, item.property, item.min, item.max);
            }


            item.controller = controller;

            controller.onChange(function (value) {
                if (item.controller.constructor.name == "StringController"){
                    _this._onChangeTask.delay(250, null, null, [item, value]);
                } else {
                    _this._onChangeTask.delay(50, null, null, [item, value]);
                }

                if (item.values){
                  this.handleVisibility();
                }
            }.bind(this));

            controller.onFinishChange(function (value) {
                _this._onChangeTask.delay(250, null, null, [item, value]);
            });

            var foldersState = JSON.parse(sessionStorage.getItem("folders-state")) || {};

            if (foldersState[item.folder] && foldersState[item.folder].closed === false){
              folder.open();
            }

            this._controllers.push(controller);
            this._propertiesMap[item.property] = item;
        }, this);

        this.handleVisibility();
    },
    setProperty: function(propName, value){
        if (!this._propertiesMap) return;
      var prop = this._propertiesMap[propName];
      if (prop){
        prop.controller.setValue(value);
      }

      // non visual properties
      prop = this._noEditorPropertiesMap[propName];
      if (prop){
        this._settings[propName] = value;
        prop.onChange(value);
      }
    },
    onFolderClick: function(){
      var foldersState = JSON.parse(sessionStorage.getItem("folders-state")) || {};
      foldersState[this.folderName] = {
        closed:this.folder.closed
      };
      sessionStorage.setItem("folders-state", JSON.stringify(foldersState));
    },
    handleVisibility: function(items){
      Ext.iterate(this._propertiesMap, function(propertyName, property){
        if (property.visibleIf){
          var targetPropertyName = property.visibleIf.split("=")[0];
          var targetProperty = this._propertiesMap[targetPropertyName] || this._noEditorPropertiesMap[targetPropertyName];

          var targetPropertyValues = property.visibleIf.split("=")[1].split("|");
          var visible = false;

          targetPropertyValues.forEach(function(targetPropertyValue){
            if (targetProperty){
              if (targetProperty.controller){
                if (targetProperty.controller.getValue() == targetPropertyValue) visible = true;
              } else {
                if (targetProperty.defaultValue == targetPropertyValue) visible = true;
              }
            }
          });

          if (!visible){
            property.controller.domElement.parentNode.parentNode.style.display = "none";
          } else {
            property.controller.domElement.parentNode.parentNode.style.display = "block";
          }
        }
      }, this);
    },
    appendSpectrumController: function (controller, settings) {
        var input = controller.domElement;
        var spectrum = $(input).spectrum({
            color: controller.getValue(),
            showPalette: true,
            showPaletteOnly: true,
            togglePaletteOnly: true,
            preferredFormat: "hex",
            maxSelectionSize:8,
            cancelText: "Cancel",
            showInput: true,
            chooseText: "OK",
            showAlpha: true,
            togglePaletteMoreText: 'All Colors...',
            controller:controller,
            togglePaletteLessText: 'All Colors...',
            localStorageKey: false,
            clickoutFiresChange: true,
            palette: Datamatic.view.widgets.DataEditorColorPicker.MATERIAL_PALETTE,
            showSelectionPalette: false,
            selectionPalette:[],
            move: function (color) {
                controller.setValue(color.toString());
            },
            change: function (color) {
                controller.setValue(color.toString());
                $(input).spectrum("hide");
            }
        });
    },
    clearProperties: function () {
        Ext.iterate(this._controllers, function (controller) {
            if (controller instanceof dat.controllers.ColorController) {
                $(controller.domElement).spectrum("destroy");
            }

            try {
              controller.__gui.remove(controller);
            } catch (e){
              // continue
            }
        }, this);

        var gui = this._gui;

        Ext.iterate(gui.__folders, function (name, value) {
            gui.__folders[name].close();
            gui.__ul.removeChild(gui.__folders[name].domElement.parentNode);
            delete gui.__folders[name];
        }, this);
    },
    __onRender: function () {
        var ct = this.body.first().first().dom;
        this._gui = new dat.GUI({
            autoPlace: false
        });
        ct.appendChild(this._gui.domElement);
    }
});
