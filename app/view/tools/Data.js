Ext.define('Datamatic.view.tools.Data', {
    extend: 'Ext.panel.Panel',
    xtype: 'data',
    alias: 'tools.Data',
    itemId: "data-view",
    split: true,
    cls: "data-panel",
    layout: "border"    
});
