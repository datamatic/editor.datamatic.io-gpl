Ext.define('Datamatic.view.googledrive.PublishSettingsWindow', {
    extend: 'Ext.window.Window',
    xtype: 'publish-settings-window',
    itemId:'publish-settings-window',
    closeAction: "hide",
    title: 'Publish settings',
    modal: true,
    frame: true,
    buttonAlign:'left',
    resizable:false,
    width: 700,
    height:400,
    cls:'datamatic-publish-settings-window',
    items: [
        {
            labelAlign: 'left',
            width:580,
            height:250,
            xtype:'htmleditor',
            enableFont:false,
            enableFontSize:false,
            enableColors:false,
            enableLinks:false,
            enableLists:true,
            enableAlignments:false,
            enableSourceEdit:true,
            cls:'datamatic-publish-settings-description',
            fieldLabel: 'Description',
            name: 'publish-settings-description'
        }
    ],

    show: function(description){
        //TODO: move this either into data editor or window subclass
        //Ext.ComponentQuery.query('#data-editor')[0].deselect();
        this.callParent();
        this._descriptionField.focus(true, 50);
        this._descriptionField.setValue(description);
        this._okBtn.enable();
    },

    handleChange: function(){
        if (this._descriptionField.getValue()){
            Datamatic.drive.setPublishSettings(this._descriptionField.getValue());
            this.hide();
        }
    },
    initComponent: function () {
        this.buttons = [
            this._okBtn = new Ext.button.Button({
                text: 'OK',
                cls:'google-btn',
                scope:this,
                handler: this.handleChange
            }),
            {
                text: 'Cancel',
                margin:'0 0 0 7',
                cls:'google-btn google-btn-secondary',
                scope:this,
                handler: function () {
                    this.hide();
                }
            }
        ];

        this.callParent();
        this._descriptionField = this.items.getAt(0);
        this._descriptionField.on("change", function(){
            if (!this._descriptionField.getValue()){
                this._okBtn.disable();
            } else {
                this._okBtn.enable();
            }
        }, this);
    }
});
