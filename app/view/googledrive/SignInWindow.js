Ext.define('Datamatic.view.googledrive.SignInWindow', {
    extend: 'Ext.window.Window',
    xtype: 'google-signin-window',
    itemId:'google-signin-window',
    cls:'datamatic-signin-window',
    title: 'Please sign in with your Google Account',
    closable:false,
    modal:true,
    resizable:false,
    frame:true,
    width: 375,
    height:150,
    buttonAlign:'center',
    bodyPadding: 10,
    items:[{
        xtype: "button",
        text: "Sign In",
        id: "google-sign-in-btn",
        cls: "google-sign-in-btn"
    }],
    initComponent: function () {
        this.callParent();
        this.render(document.body);
    }
});
