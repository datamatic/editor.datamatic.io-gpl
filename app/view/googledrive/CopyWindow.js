Ext.define('Datamatic.view.googledrive.CopyWindow', {
    extend: 'Ext.window.Window',
    xtype: 'copy-window',
    closeAction: "hide",
    title: 'Copy document',
    modal: true,
    frame: true,
    buttonAlign:'left',
    resizable:false,
    width: 425,
    height:210,
    cls:'datamatic-copy-window',
    defaultType: 'textfield',
    items: [
        {
            labelAlign: 'top',
            width:320,
            cls:'datamatic-copy-field',
            fieldLabel: 'Enter a new document name:',
            name: 'copy-file-input'
        }
    ],

    show: function(fileTitle){
        //TODO: move this either into data editor or window subclass
        Ext.ComponentQuery.query('#data-editor')[0].deselect();
        this._copyField.enable();
        this._copyField.setValue(fileTitle);
        this._fileTitle = fileTitle;
        this.callParent();
        this._copyField.focus(true, 50);
        this._okBtn.enable();
    },

    afterRender: function(){
        var keyMap = this.getKeyMap();
        keyMap.on(13, this.handleCopy, this);
        this.callParent();
    },
    handleCopy: function(){
        if (this._copyField.getValue()){
            this._okBtn.disable();
            this._copyField.disable();
            this.hide();
            Datamatic.drive.doCopy(this._copyField.getValue(), function(fileID){

            }.bind(this),
            function(message){
              alert(message);
              this._okBtn.enable();
            }.bind(this));

        }
    },
    initComponent: function () {
        this.buttons = [
            this._okBtn = new Ext.button.Button({
                text: 'OK',
                cls:'google-btn',
                scope:this,
                handler: this.handleCopy
            }),
            {
                text: 'Cancel',
                margin:'0 0 0 7',
                cls:'google-btn google-btn-secondary',
                scope:this,
                handler: function () {
                    this.hide();
                }
            }
        ];

        this.callParent();
        this._copyField = this.items.getAt(0);
        this._copyField.on("change", function(){
            if (!this._copyField.getValue()){
                this._okBtn.disable();
            } else {
                this._okBtn.enable();
            }
        }, this);
    }
});
