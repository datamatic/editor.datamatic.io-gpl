Ext.define('Datamatic.view.googledrive.PublishWindow', {
    extend: 'Ext.window.Window',
    xtype: 'publish-window',
    itemId:'publish-window',
    closeAction: "hide",
    title: 'Publish your visualization',
    modal: true,
    frame: true,
    buttonAlign:'left',
    resizable:false,
    width: 600,
    height:240,
    cls:'datamatic-publish-window',
    items: [

    ],

    show: function(fileTitle){
        //TODO: move this either into data editor or window subclass
        Ext.ComponentQuery.query('#data-editor')[0].deselect();
        this.setTitle("Publishing...");
        this.addCls("publishing");
        this._doneBtn.hide();
        this.callParent();
        this.bodyCt.setHtml("");
    },

    onPublishEvent: function(event){
      if (event.progress == "100%"){
        //this.removeCls("publishing");
        this.setTitle("Your visualization is public");
        this.removeCls("publishing");
        this._update(event);
        this._doneBtn.show();
      }
    },
    //TODO: move this into its own controller
    _getAPICode: function(event, settings){
        var root = JSON.parse(Datamatic.drive.getModel().getRoot().get("data")).children[0];

        var trimRow = function(row){
          var index = row.length;
          while (index && row[index-1] === "") {
            index--;
            row.length = index; 
          }
        };

        var mapRow = function(row){
            var result = [];

            // map field names to array
            Object.keys(row).forEach(function(cellName){
              var index = Handsontable.helper.spreadsheetColumnIndex(cellName);
              if (index > -1){
                result[index] = row[cellName];
              }
            });

            // remove empty cells from the end of row
            trimRow(result);

            // append children if available
            var children = row.children?row.children.map(mapRow):undefined;
            if (children && children.length) {
              result.push(children);
            }

            return result;
        };

        var data = root.children.map(mapRow);
        var dataStr = JSON.stringify(data, false, 4);
        dataStr = dataStr.split("\n").map(function(row){
            return "        " + row;
        }).join("\n").trim();

        var templateID = Ext.ComponentQuery.query('#editor-output')[0].getTemplateID();
        var apiURL = Ext.manifest.vars.RUNTIME[templateID].API_URL;

        var variables = {
            userID:event.userID,
            docID:event.docID,
            data:dataStr,
            apiUrl:apiURL,
            width:settings.width,
            height:settings.height
        }

        var tpl = new Ext.XTemplate(
        '<html>\n'+
        '<body>\n'+
        '<script type="text/javascript" src="{apiUrl}"></script>\n'+
        '<script>\n'+
        '   var datamatic = new Datamatic("{userID}");\n'+
        '      var chart = datamatic.chart("{docID}", {\n'+
        '         width:{width},\n'+
        '         height:{height}\n'+
        '      });\n'+
        '      chart.render(document.body).then(function(){\n'+
        '          chart.setData({data});\n'+
        '      });\n'+
        '</script>\n'+
        '</body>\n'+
        '</html>\n');

        return tpl.apply(variables);
    },
    _update: function(event){
      var settings = Ext.ComponentQuery.query('#editor-output')[0].getSettings();
      var templateID = Ext.ComponentQuery.query('#editor-output')[0].getTemplateID();
      var embeddedUrl = Ext.manifest.vars.RUNTIME[templateID].EMBEDDED_URL;

      var data = Ext.apply(event, {
        printUrl: event.shortUrl.replace("http://", ""),
        embedUrl:embeddedUrl+"#"+event.longUrl.split("#")[1],
        computedWidth:settings.left + settings.width,
        apiCode:this._getAPICode(event, settings),
        computedHeight:settings.top + settings.height + 3
      });

      var tpl = new Ext.XTemplate(
          '<div class="pub-dialog-tab-bar goog-tab-bar goog-tab-bar-top">'+
            '<div class="pub-dialog-tab goog-tab goog-tab-selected">Link</div>'+
            '<div class="pub-dialog-tab goog-tab">Embed</div>'+
            '<div class="pub-dialog-tab goog-tab">API</div>'+
          '</div>'+
          '<div class="datamatic-publish-window-url-list">'+
            '<div class="datamatic-publish-window-url-header">'+
              '<div class="datamatic-publish-window-url-header-url">URL</div>'+
              '<div class="datamatic-publish-window-url-header-views">VIEWS</div>'+
              '<div class="datamatic-publish-window-url-header-share">SHARE</div>'+
            '</div>'+
            '<div class="datamatic-publish-window-url-row">'+
              '<div class="datamatic-publish-window-url-row-url"><a href="{shortUrl}" target="_blank">{printUrl}</a></div>'+
              '<div class="datamatic-publish-window-url-row-views">{analytics.allTime.shortUrlClicks}</div>'+
              '<div class="datamatic-publish-window-url-row-share">'+
                '<div class="g-plus" style="margin-right:3px;" data-action="share" data-annotation="bubble" data-href="{shortUrl}"></div>'+
                '<div class="fb-share-button" data-href="{shortUrl}" data-width="200" data-type="button_count"></div>' +
                '<a href="https://twitter.com/share" class="twitter-share-button" data-url="{shortUrl}"></a>' +
              '</div>'+
            '</div>'+
          '</div>'+
          '<div class="datamatic-publish-window-embed" style="display:none">'+
            '<textarea id="pub-dialog-embed-code" rows="3" cols="60" class="pub-dialog-embed-code jfk-textinput" readonly="readonly" style="">'+
              '<iframe src="{embedUrl}" frameborder="0" width="{computedWidth}" height="{computedHeight}" allowtransparency="true"></iframe>'+
            '</textarea>'+
          '</div>'+
          '<div class="datamatic-publish-window-api" style="display:none">'+
            '<textarea id="pub-dialog-embed-api" rows="3" cols="60" class="pub-dialog-api-code jfk-textinput" readonly="readonly" style="">'+
              '{apiCode}'+
            '</textarea>'+
          '</div>'
      );

      tpl.overwrite(this.bodyCt, data);
      twttr.widgets.load();
      FB.XFBML.parse();
      gapi.plus.go();

      $('.pub-dialog-tab').click(function() {
        $('.pub-dialog-tab').removeClass('goog-tab-selected');
        $(this).addClass('goog-tab-selected');
        switch ($(this).text()) {
          case "Link":$(".datamatic-publish-window-url-list").show();$(".datamatic-publish-window-embed").hide();$(".datamatic-publish-window-api").hide();ga('send', 'event', 'publish', 'link'); return;
          case "Embed":$(".datamatic-publish-window-url-list").hide();$(".datamatic-publish-window-api").hide();$(".datamatic-publish-window-embed").show();$(".pub-dialog-embed-code").select(); ga('send', 'event', 'publish', 'embed'); return;
          case "API":$(".datamatic-publish-window-url-list").hide();$(".datamatic-publish-window-embed").hide();$(".datamatic-publish-window-api").show();$(".pub-dialog-api-code").select(); ga('send', 'event', 'publish', 'api'); return;
        }
      });

      $(".pub-dialog-embed-code").mouseup(function () {
          $(this).select();
          return false;
      });
    },

    afterRender: function(){
        var keyMap = this.getKeyMap();
        keyMap.on(13, function(){
          this.hide();
        }, this);
        this.bodyCt = this.body.down(".x-autocontainer-innerCt");
        this.callParent();
    },

    initComponent: function () {
        this.buttons = [
          this._doneBtn = new Ext.button.Button({
                text: 'Done',
                margin:'0 0 0 0',
                cls:'google-btn',
                scope:this,
                handler: function () {
                    this.hide();
                }
            }
        )];

        $.ajax({ url: '//platform.twitter.com/widgets.js', dataType: 'script', cache:true});
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $.ajax({ url: '//apis.google.com/js/platform.js', dataType: 'script', cache:true});
        this.callParent();
    }
});
