Ext.define('Datamatic.view.googledrive.RenameWindow', {
    extend: 'Ext.window.Window',
    xtype: 'rename-window',
    closeAction: "hide",
    title: 'Rename document',
    modal: true,
    frame: true,
    buttonAlign:'left',
    resizable:false,
    width: 425,
    height:210,
    cls:'datamatic-rename-window',
    defaultType: 'textfield',
    items: [
        {
            labelAlign: 'top',
            width:320,
            cls:'datamatic-rename-field',
            fieldLabel: 'Enter a new document name:',
            name: 'rename-file-input'
        }
    ],

    show: function(fileTitle){
        //TODO: move this either into data editor or window subclass
        Ext.ComponentQuery.query('#data-editor')[0].deselect();

        this._renameField.setValue(fileTitle);
        this.callParent();
        this._renameField.focus(true, 50);
        this._okBtn.enable();
    },

    afterRender: function(){
        var keyMap = this.getKeyMap();
        keyMap.on(13, this.handleRename, this);
        this.callParent();
    },
    handleRename: function(){
        if (this._renameField.getValue()){
            Datamatic.drive.doRename(this._renameField.getValue());
            this.hide();
        }
    },
    initComponent: function () {
        this.buttons = [
            this._okBtn = new Ext.button.Button({
                text: 'OK',
                cls:'google-btn',
                scope:this,
                handler: this.handleRename
            }),
            {
                text: 'Cancel',
                margin:'0 0 0 7',
                cls:'google-btn google-btn-secondary',
                scope:this,
                handler: function () {
                    this.hide();
                }
            }
        ];

        this.callParent();
        this._renameField = this.items.getAt(0);
        this._renameField.on("change", function(){
            if (!this._renameField.getValue()){
                this._okBtn.disable();
            } else {
                this._okBtn.enable();
            }
        }, this);
    }
});
