/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Datamatic.view.main.MainViewport', {
    extend: 'Ext.container.Container',
    cls:"datamatic-loading",
    itemId:"main-viewport",
    xtype: 'main-viewport',
    viewModel: {
        type: 'mainViewport'
    },

    layout: {
        type: 'border'
    },

    items: [
        {
            xtype:"main-header",
            height:60,
            region:"north"
        },
        {
            xtype:"main-toolbar",
            height:36,
            border:0,
            region:"north"
        },
        {
            xtype:"panel",
            region:'center',
            layout: {
                type: 'border'
            },
            items:[
                {
                    xtype: 'data-editor-breadcrumb',
                    height:35,
                    split:false,
                    style:"top:0px",
                    hidden:true,
                    region:"north"
                },
                {
                    xtype:"data",
                    split:true,
                    width:500,
                    margin:'0 0 0 0',
                    items: [
                        {
                            xtype: "dataTree",
                            hidden: true,
                            split:false,
                            region: "west"
                        },
                        {
                            xtype: "dataEditor",
                            split:false,
                            height:300,
                            width:"100%",
                            region: "east"
                        }
                    ],
                    region:"west"
                },
                {
                    xtype:"panel",
                    split:true,
                    region:"center",
                    margin:'0 0 0 0',
                    cls:"datamatic-horizontal-tools",
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items:[
                      {
                          xtype:"code",
                          flex:1,
                          minWidth:250,
                          hidden:true
                      },
                      {
                        xtype:'splitter',
                        collapseOnDblClick: false,
                        hidden:true,
                        itemId:'result-view-splitter'
                      },
                      {
                          xtype:"result",
                          flex:1
                      }
                    ]
                }
                // ,{
                //     xtype:"data",
                //     split:true,
                //     height:300,
                //     margin:'-15 0 0 0',
                //     items: [
                //         {
                //             xtype: 'data-editor-breadcrumb',
                //             height:35,
                //             split:false,
                //             region:"north"
                //         },
                //         {
                //             xtype: "dataTree",
                //             hidden: true,
                //             split:false,
                //             region: "west"
                //         },
                //         {
                //             xtype: "dataEditor",
                //             split:false,
                //             height:300,
                //             width:"100%",
                //             region: "east"
                //         }
                //     ],
                //     region:"south"
                // }

            ]
        },
        {
            xtype:"properties",
            width:325,
            split:true,
            margin:"-36 0 0 0",
            region:"east"
        }
    ]
});
