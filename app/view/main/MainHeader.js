Ext.define('Datamatic.view.main.MainHeader', {
    extend: 'Ext.panel.Panel',
    requires: 'Datamatic.view.main.MenuToggleViewItem',
    xtype: 'main-header',
    autoEl: {
        cls: "main-header",
        children: [{
            cls: "main-logo",
            children: [{
                cls: "datamatic-logo"
            }, {
                cls: "drive-logo",
                children: [{
                    tag: "a",
                    target: "_blank",
                    href: "https://drive.google.com"
                }],
                "data-qtip": "Open Google Drive",
                "data-qclass": "quick-tip open-drive-tooltip"
            }]
        }, {
            cls: "file-title",
            html: "",
            id: "datamatic-file-title"
        }, {
            cls: "user-info",
            hidden: true,
            id: "google-user-info",
            html: "jarben@aviarc.com"
        }]
    },
    items: [{
        xtype: "button",
        text: "Share",
        handler: function () {
            rtclient.share.showSettingsDialog();
        },
        hidden: true,
        id: "google-share-btn",
        cls: "google-btn main-header-share-btn",
        iconCls: "google-svg-icon google-share-icon"
    },{
        xtype: "button",
        itemId:"google-publish-btn",
        text: "Publish",
        handler: function () {
            Datamatic.drive.publish();
        },
        hidden:true,
        id: "google-publish-btn",
        cls: "google-btn main-header-publish-btn",
        iconCls: "google-svg-icon google-publish-icon"
    }, {
        xtype: "button",
        text: "Sign Out",
        /*href:"https://accounts.google.com/logout",*/
        handler: function () {
            Datamatic.drive.signOut();
            ga('send', 'event', 'sign-out', 'click');
        },
        hidden: true,
        id: "google-sign-out-btn",
        cls: "google-btn google-btn-secondary main-header-sign-out-btn"
    }
    ],
    bbar: {
        xtype: 'toolbar',
        dock: 'bottom',
        items: [{
            text: 'File',
            disabled: true,
            itemId: "file-menu",
            menu: {
                xtype: "menu",
                minWidth: 250,
                items: [{
                        text: "Share...",
                        href:"javascript:void(0)",
                        handler: function () {
                            rtclient.share.showSettingsDialog();
                            ga('send', 'event', 'file-menu', 'share');
                        }
                    },
                    "-",
                    {
                        text: "Open...",
                        href:"javascript:void(0)",
                        handler: function () {
                            Datamatic.openFile();
                            ga('send', 'event', 'file-menu', 'open');
                        }
                    }, {
                        text: "Rename...",
                        href:"javascript:void(0)",
                        handler: function () {
                            Datamatic.drive.renameFile();
                            ga('send', 'event', 'file-menu', 'rename');
                        }
                    },
                    {
                        text: "Make a copy...",
                        href:"javascript:void(0)",
                        handler: function () {
                            Datamatic.drive.copyFile();
                        }
                      }
                    ,
                    {
                        text: "Publish settings...",
                        href:"javascript:void(0)",
                        hidden:true,
                        itemId:"publish-settings-menu-item",
                        handler: function () {
                            Datamatic.drive.openPublishSettings();
                            ga('send', 'event', 'file-menu', 'publish');
                        }
                    },
                    "-", {
                        text: "Download as",
                        href:"javascript:void(0)",
                        itemId: "download-as-menu-item",
                        menu: [
                          {
                              text: "PNG image (transparent)",
                              href:"javascript:void(0)",
                              handler: function () {
                                  Datamatic.getOutput().download("png");
                                  ga('send', 'event', 'file-menu', 'download', 'png');
                              }
                          },{
                            text: "JPG image",
                            href:"javascript:void(0)",
                            handler: function () {
                                Datamatic.getOutput().download("jpg");
                                ga('send', 'event', 'file-menu', 'download', 'jpg');
                            }
                        }]
                    }
                ]
            }
        }, {
            text: 'Edit',
            menu: {
                xtype: "menu",
                minWidth: 200,
                items: [{
                        text: "Undo",
                        id: "main-menu-undo-btn",
                        disabled: true,
                        iconCls: "docs-icon docs-icon-undo"
                    }, {
                        text: "Redo",
                        disabled: true,
                        id: "main-menu-redo-btn",
                        iconCls: "docs-icon docs-icon-redo"
                    },
                    "-", {
                        text: "Cut",
                        iconCls: "docs-icon docs-icon-cut",
                        handler: function () {
                            Ext.ComponentQuery.query('#data-editor')[0].cut();
                            ga('send', 'event', 'file-menu', 'edit', 'cut');
                        }
                    }, {
                        text: "Delete",
                        iconCls: "docs-icon docs-icon-trash",
                        handler: function () {
                            Ext.ComponentQuery.query('#data-editor')[0].remove();
                            ga('send', 'event', 'file-menu', 'edit', 'delete');
                        }
                    },{
                        text: "Copy",
                        iconCls: "docs-icon docs-icon-copy",
                        handler: function () {
                            Ext.ComponentQuery.query('#data-editor')[0].copy();
                            ga('send', 'event', 'file-menu', 'edit', 'copy');
                        }
                    }, {
                        text: "Paste",
                        iconCls: "docs-icon docs-icon-paste",
                        handler: function () {
                            Ext.ComponentQuery.query('#data-editor')[0].paste();
                            ga('send', 'event', 'file-menu', 'edit', 'paste');
                        }
                    }
                ]
            }
        }, {
            text: 'View',
            menu: {
                xtype: "menu",
                minWidth: 200,
                items: [{
                    text: "Show Data",
                    xtype: "menu-toggle-item",
                    viewId: "data-view"
                }, {
                    text: "Show Properties",
                    xtype: "menu-toggle-item",
                    viewId: "properties-view"
                }]
            }
        },
        {
            text: 'Help',
            id: "datamatic-help-btn",
            menu: {
                xtype: "menu",
                minWidth: 200,
                items: [{
                        text: "Send feedback",
                        handler: function () {
                            UserVoice.push(['show', {
                                mode: 'contact',
                                target: "#datamatic-help-btn"
                            }]);

                            ga('send', 'event', 'help', 'concact');
                        }
                    }
                ]
            }
        }]
    }
});
