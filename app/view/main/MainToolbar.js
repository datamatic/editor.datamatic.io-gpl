Ext.define('Datamatic.view.main.MainToolbar', {
    extend: 'Ext.panel.Panel',
    cls:"datamatic-main-toolbar",
    xtype: 'main-toolbar',
    itemId:'main-toolbar',

    tbar: {
		xtype: 'toolbar',
        trackMenus:false,
		layout:{
		    type: 'hbox',
		    //TODO: replace this by css
		    defaultMargins:{
		        top: 0,
		        right: -8,
		        bottom: 0,
		        left: 0
		    }
		},
		items:[
	        {
	        	cls:"x-btn-icon",
	        	id: "main-toolbar-undo-btn",
	        	disabled:true,
	        	iconCls:"docs-icon docs-icon-undo"
	        },
	        {
	        	cls:"x-btn-icon",
	        	disabled:true,
	        	id: "main-toolbar-redo-btn",
	        	iconCls:"docs-icon docs-icon-redo"
	        },
          "-",

          //TODO: make ToolbarToggleViewItem.js
          {
            text: 'Data',
            handler: function(){
              if (this.pressed){
                Ext.ComponentQuery.query('#data-view')[0].hide();
              } else {
                Ext.ComponentQuery.query('#data-view')[0].show();
              }
            },
            listeners: {
              render: function(btn){
                var dataView = Ext.ComponentQuery.query('#data-view')[0];
                dataView.on("hide", function(){
                  btn.setPressed(false);
                }, this);

                dataView.on("show", function(){
                  btn.setPressed(true);
                }, this)
              }
            },
            margin: '0 0 0 0',
            pressed:true
          },

        {
              text: 'Code',
              itemId:"main-toolbar-code-btn",
              handler: function(){
                if (this.pressed){
                  Ext.ComponentQuery.query('#code-view')[0].hide();
                } else {
                  Ext.ComponentQuery.query('#code-view')[0].show();
                }
              },
              listeners: {
                render: function(btn){
                  var codeView = Ext.ComponentQuery.query('#code-view')[0];
                  codeView.on("hide", function(){
                    Ext.ComponentQuery.query('#result-view-splitter')[0].hide();
                    btn.setPressed(false);
                  }, this);

                  codeView.on("show", function(){
                    Ext.ComponentQuery.query('#result-view-splitter')[0].show();
                    btn.setPressed(true);
                  }, this)
                }
              },
              margin: '0 0 0 4',
              pressed:false,
              hidden:true
            },          

        // {
        //       text: 'Preview',
        //       itemId:"main-toolbar-preview-btn",
        //       handler: function(){
        //         if (this.pressed){
        //           Ext.ComponentQuery.query('#result-view')[0].hide();
        //         } else {
        //           Ext.ComponentQuery.query('#result-view')[0].show();
        //         }
        //       },
        //       listeners: {
        //         render: function(btn){
        //           var resultView = Ext.ComponentQuery.query('#result-view')[0];
        //           resultView.on("hide", function(){
        //             btn.setPressed(false);
        //           }, this);

        //           resultView.on("show", function(){
        //             btn.setPressed(true);
        //           }, this)
        //         }
        //       },
        //       margin: '0 0 0 4',
        //       pressed:true,
        //       hidden:false
        //     },    

	        {
		        text: 'Properties',
             margin: '0 0 0 4',
		        handler: function(){
		        	if (this.pressed){
		        		Ext.ComponentQuery.query('#properties-view')[0].hide();
		        	} else {
		        		Ext.ComponentQuery.query('#properties-view')[0].show();
		        	}
		        },
		        listeners: {
		        	render: function(btn){
		        		var propertiesView = Ext.ComponentQuery.query('#properties-view')[0];
		        		propertiesView.on("hide", function(){
		        			btn.setPressed(false);
		        		}, this);

		        		propertiesView.on("show", function(){
		        			btn.setPressed(true);
		        		}, this)
		        	}
		        },
		        pressed:true
	        }
	    ]
    }
});
