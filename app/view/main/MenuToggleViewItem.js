Ext.define('Datamatic.view.main.MenuToggleViewItem', {
    extend: 'Ext.menu.CheckItem',
    checked:false,
    initComponent: function () {
        this.callParent();
        this.on("render", function (item) {
            var view = Ext.ComponentQuery.query("#" + this.viewId)[0];
            if (view.isVisible()) this.setChecked(true);

            view.on("hide", function () {
                item.setChecked(false);

                var hideParent = true;
                view.ownerCt.items.each(function(item){
                  if (item.isVisible()) hideParent = false;
                });

                if (hideParent){
                  view.ownerCt.hide();
                }

                ga('send', 'event', 'view', 'hide', this.viewId);
            }, this);

            view.on("show", function () {
                item.setChecked(true);
                view.ownerCt.show();
                ga('send', 'event', 'view', 'show', this.viewId);
            }, this)
        }, this);
    },
    handler: function () {
        if (this.checked) {
            Ext.ComponentQuery.query("#" + this.viewId)[0].show();
        } else {
            Ext.ComponentQuery.query("#" + this.viewId)[0].hide();
        }
    },
    xtype: 'menu-toggle-item'
});
