/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('Datamatic.view.main.MainViewportModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.mainViewport',

    data: {
        name: 'Datamatic'
    }

    //TODO - add data, formulas and/or methods to support your view
});