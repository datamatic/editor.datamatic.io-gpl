(function(){
Ext.define('Datamatic.model.TreeDataModel', {
	extend: 'Ext.data.TreeModel',
    copy: function(newId, deep) {
        var me = this,
            // FIX: dont' pass arguments as it would throw missing session error
            result = me.callParent([newId]),
            len = me.childNodes ? me.childNodes.length : 0,
            i;

        // Move child nodes across to the copy if required
        if (deep) {
            for (i = 0; i < len; i++) {
                result.appendChild(me.childNodes[i].copy(undefined, true));
            }
        }
        return result;
    },
    fields: []
});
})();
