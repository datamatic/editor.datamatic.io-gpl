Ext.define('Datamatic.controller.PublishController', {
    extend: 'Ext.app.Controller',
    getUserDetails: function(){
        var result = {};
        var authDetails = Datamatic.drive.authDetails;
        if (!authDetails){
            result = "Error: can't publish when user is not authenticated";
        }
        if (!authDetails.verified_email){
            result = "Error: can't publish when user's email is not verified";
        }

        result.name = authDetails.name;
        result.email = authDetails.email;
        result.id = authDetails.id;

        return result;
    },
    //TODO: break this into shortener controller
    getShortURL: function(longUrl, callback){
      gapi.client.load('urlshortener', 'v1').then(function(){
        gapi.client.setApiKey(Ext.manifest.vars.GOOGLE_SHORTENER_API_KEY);
        var root = Datamatic.drive.getModel().getRoot();
        var shortUrl = root.get("shortURL");

        if (shortUrl){
          gapi.client.urlshortener.url.get({
              'shortUrl': shortUrl,
              'projection':'FULL'
            }).then(function(ev){
               // only use existing if longUrl matches given longUrl
               // this can happen when there are changes in publishing
               // mechanism/url, we need to republish legacy visualizations
               // as new files
               if (ev.result && longUrl == ev.result.longUrl){
                   callback(ev.result);
               } else {
                   this.createShortURL(longUrl, callback);
               }
           }.bind(this));
        } else {
          this.createShortURL(longUrl, callback);
        }
      }.bind(this));
    },
    createShortURL: function(longUrl, callback){
      gapi.client.load('urlshortener', 'v1').then(function(){
        gapi.client.urlshortener.url.insert({
           'longUrl': longUrl
         }).then(function(ev){
            gapi.client.urlshortener.url.get({
                'shortUrl': ev.result.id,
                'projection':'FULL'
              }).then(function(ev){
                 Datamatic.drive.getModel().getRoot().set("shortURL", ev.result.id)
                 callback(ev.result);
              });
         });
      });
    },
    getCompiledCode: function(root){
        var code = this.getCode();
        var compiledCode = Babel.transform(code, { presets: ['es2015-no-commonjs'] }).code;
        return compiledCode;
    },
    encodeProperty: function(property){
        return btoa(unescape(encodeURIComponent(property)));
    },
    getCode: function(){
        return Datamatic.drive.getModel().getRoot().get("code").getText();
    },
    getPublishData: function(folderId, shortUrl){
      var root = Datamatic.drive.getModel().getRoot();
      var userDetails = this.getUserDetails();
      var code = this.getCode();
      var data = {
          template: root.get("template").getText(),
          data: this.encodeProperty(root.get("data").getText()),
          title:escape(Datamatic.drive.getFileDetails().title),
          folderId:folderId,
          version:"3",
          userName:userDetails.name,
          userId:userDetails.id,
          shortUrl: shortUrl,
          forkUrl:Ext.manifest.vars.FORK_URL,
          description:this.encodeProperty(root.get("description")),
          code:this.encodeProperty(this.getCode()),
          parentForkId:root.get("parentForkId"),
          settings: this.encodeProperty(root.get("settings").getText()),
          fileId:Datamatic.drive.getFileDetails().id
      };

      var compiledCode = this.getCompiledCode();

      if (compiledCode != code){
          data.compiledCode = this.encodeProperty(compiledCode);
      }

      return data;
    },
    getSampleData: function(folderId, shortUrl){
      var root = Datamatic.drive.getModel().getRoot();
      var data = {
          template: root.get("template").getText(),
          data: root.get("data").getText(),
          title:Datamatic.drive.getFileDetails().title,
          folderId:folderId,
          version:"2",
          shortUrl: shortUrl,
          code:btoa(unescape(encodeURIComponent(root.get("code").getText()))),
          parentForkId:root.get("parentForkId"),
          settings: root.get("settings").getText(),
          fileId:Datamatic.drive.getFileDetails().id
      };

      return data;
    },

    escapeJSONObject: function(obj){
      Ext.iterate(obj, function(key, value){
        if (typeof obj[key] == "object"){
          this.escapeJSON(obj[key]);
        } else {
          obj[key] = escape(value);
        }
      }, this);
      return obj;
    },
    publish: function (success, failure) {
      var templateID = Ext.ComponentQuery.query('#editor-output')[0].getTemplateID();
      var viewURL = Ext.manifest.vars.RUNTIME[templateID].VIEW_URL;
            
      var userDetails = this.getUserDetails();
      // error
      if (typeof userDetails == "string"){
          failure(userDetails);
          return;
      }

      var dataFolder = Ext.manifest.vars.DATA_FOLDER;
      var viewFolder = dataFolder + "/"+ userDetails.id;
      var docID = Datamatic.drive.getFileDetails().id;
      var id = userDetails.id+"/"+docID;
      var longUrl = viewURL+"#id=" + id;
      this.getShortURL(longUrl, function(result){
        this.getPublishedFile(viewFolder, result.id, function (file) {
           success({
             progress:"100%",
             longUrl:longUrl,
             docID:docID,
             userID:userDetails.id,
             id: id,
             analytics:result.analytics,
             shortUrl:result.id
           });
        }.bind(this), function(message){
          failure(message);
        });
      }.bind(this));
    },
    getPublishedFile: function (folderId, shortUrl, success, failure) {
        var root = Datamatic.drive.getModel().getRoot();
        var publishedFileID = root.get("publishedFile");
        var data = this.getPublishData(folderId, shortUrl);

        this.writeFile(folderId, data, function(result){
            root.set("publishedFile", result.id);
            success(result);
        }, failure);
    },

    writeFile: function (folderId, data, success, failure) {
      var content = JSON.stringify(data);
      var userDetails = this.getUserDetails();

      $.ajax({
        type: "POST",
        url: Ext.manifest.vars.PUBLISH_URL+"?code="+Ext.manifest.vars.PUBLISH_KEY,
        beforeSend: function (xhr) {
          xhr.setRequestHeader("Authorization", "Bearer "+gapi.auth.getToken().access_token);
        },
        data: {
          content: content,
          access_token:gapi.auth.getToken().access_token,
          id: data.fileId,
          userId: data.userId,
          //legacy
          folder:folderId,
          email:userDetails.email
        },
        error: function(resp, type, message){
          alert(message);
        }.bind(this),
        success: function(resp, type){
          if (resp.indexOf("error: ") == 0){
            var error = resp.split("error: ")[1];
            failure(error);
          } else if (resp == "success"){
            success({
              id: data.fileId
            });
          }
        }.bind(this)
      });
    }
});
