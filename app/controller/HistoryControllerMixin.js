 Ext.define('Datamatic.controller.HistoryControllerMixin', {
	 onLaunch: function () {
		 var undoBtn = this.getUndoButton();
    	 var redoBtn = this.getRedoButton();
         
         setTimeout(function(){
             Datamatic.drive.on("undoRedoStateChanged", function(canUndo, canRedo){
                canUndo?undoBtn.enable():undoBtn.disable();
                canRedo?redoBtn.enable():redoBtn.disable();
             });
             
             Datamatic.drive.on("documentClosed", function(canUndo, canRedo){
                undoBtn.disable();
                redoBtn.disable();
             });
         });
    	 
    	 undoBtn.on("click", function(){
    		 Datamatic.drive.undo();
    	 });
    	 
    	 redoBtn.on("click", function(){
    		 Datamatic.drive.redo();
    	 });
     }
 });