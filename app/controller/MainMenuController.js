Ext.define('Datamatic.controller.MainMenuController', {
    extend: 'Ext.app.Controller',
    mixins: {
        history: 'Datamatic.controller.HistoryControllerMixin'
    },
    refs: [
       {
           ref: 'editor',
           selector: '#data-editor'
       },
       {
           ref: 'undoButton',
           selector: '#main-menu-undo-btn'
       },
       {
           ref: 'redoButton',
           selector: '#main-menu-redo-btn'
       }
    ],
    onLaunch: function(){
    	this.mixins.history.onLaunch.apply(this, arguments);
    }
});
