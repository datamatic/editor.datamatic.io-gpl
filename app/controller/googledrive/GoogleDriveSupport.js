var DATAMATIC_DEFAULT_TITLE = Ext.manifest.vars.DATAMATIC_DEFAULT_TITLE;

Ext.define('Datamatic.controller.googledrive.GoogleDriveSupport', {
    extend: 'Ext.app.Controller',
    requires: ['Datamatic.controller.googledrive.RealtimeOptions'],
    views: ['googledrive.RenameWindow', 'googledrive.CopyWindow', 'googledrive.PublishWindow','googledrive.PublishSettingsWindow', 'googledrive.SignInWindow'],
    refs: [{
        ref: 'renameWindow',
        selector: '#rename-window'
    }, 
    {
        ref: 'publishWindow',
        selector: '#publish-window'
    },{
        ref: 'publishSettings',
        selector: '#publish-settings-window'
    },{
        ref: 'dataTree',
        selector: '#data-tree'
    },{
        ref: 'signInWindow',
        selector: '#google-signin-window'
    }],

    openPublishSettings: function(){
        if (this._root){
            this.getPublishSettings().show(this._root.get("description"));
        }
    },

    setPublishSettings: function(description){
        if (this._root){
            this._root.set("description", description);
        }
    },

    loadForkData: function(callback){
      var forkId = this.forkId = rtclient.params['forkId'];
      var dataURL = Ext.manifest.vars.DATA_URL;
      var dataFolder = Ext.manifest.vars.DATA_FOLDER;

      d3.json(dataURL+"/"+dataFolder+"/"+forkId+".json?ts="+(new Date()).getTime(), function(root){
          this._newDocumentData = new Datamatic.document.NewDocumentData({
              data:JSON.parse(decodeURIComponent(escape(window.atob((root.data))))),
              settings:JSON.parse(decodeURIComponent(escape(window.atob((root.settings))))),
              templateID: root.template,
              code:decodeURIComponent(escape(window.atob(root.code)))
          });

          this.fireEvent("b4loadForkData", this._newDocumentData);

          root.title = unescape(root.title);

          // title
          if (root.title){
             Ext.get("datamatic-file-title").update(root.title);
             this._forkTitle = root.title;
             this.realtimeLoader.defaultTitle = root.title;
          }

          Datamatic.drive.hideLoader();
          this._forkLoaded = true;

          Datamatic.documentMgr.newDocument(this._newDocumentData);
      }.bind(this));
    },
    hasUserInfo: function(){
      return !!this._userInfo;
    },
    getUserInfo: function(authDetails){
      var df = $.Deferred();
      if (this._userInfo){
        df.resolve(this._userInfo);
      } else {
        this._requestUserInfo(authDetails.email, function(userInfo){
          this._userInfo = userInfo;
          df.resolve(this._userInfo);
      }.bind(this));
      }

      return df.promise();
    },
    _requestUserInfo: function(email, callback){
      $.post(Ext.manifest.vars.USER_INFO_URL, {access_token: gapi.auth.getToken().access_token}, function( response ) {
        // user is not registered, use free plan
        if (response.indexOf("Error:") != -1){
          callback({
            plan:"Free",
            registered:false
          });
        } else {
          var info = JSON.parse(response);
          info.registered = true;
          callback(info);
        }
      }.bind(this));
    },

    getModel: function(){
      return this._model;
    },

    authorizeWithPopup: function () {
        this.realtimeLoader.authorizer.authorizeWithPopup();
    },

    onUndoRedoStateChanged: function (ev) {
        this.fireEvent("undoRedoStateChanged", ev.canUndo, ev.canRedo);
    },

    isHistoryAction: function () {
        return this._isHistoryAction;
    },

    isDriveChange: function () {
        return this._isDriveChange;
    },
    onDriveDataChange: function (dataStr) {
        var store = Datamatic.getTreeDataStore();

        this._isDriveChange = true;
        Datamatic.getTreeDataStore().setRootNode(JSON.parse(dataStr));
        this._isDriveChange = false;

        if (this._selectionPath) {
            this.restoreSelection();
        }
    },
    publish: function(){
      // sanity
      if (this._model){
        var publishWindow = this.getPublishWindow();
        publishWindow.show();

        this.getController("PublishController").publish(function(event){
          publishWindow.onPublishEvent(event);
          ga('send', 'event', 'publish', 'published', event.shortUrl);
        }, function(message){
          publishWindow.hide();
          alert(message);
        });
      }
    },
    // restores selection when another user is editing the same document
    restoreSelection: function () {
        if (!this._selectionPath) this._selectionPath = "/0/0/0";
        var selectionPathParts = this._selectionPath.split("/");

        var afterSelection = function (selected) {
            if (!selected && selectionPathParts.length) {
                var lastIndex = selectionPathParts.pop();

                // decrement index if more than 0
                if (lastIndex > 0) {
                    lastIndex--;
                    selectionPathParts.push(lastIndex);
                } // otherwise try to select parent

                this._selectionPath = selectionPathParts.join("/");
                this.getDataTree().selectPath(this._selectionPath, 'index', null, afterSelection);
            }
        }.bind(this);

        this.getDataTree().selectPath(this._selectionPath, 'index', null, afterSelection);
    },
    onDriveSettingsChange: function (settingsStr) {
        this._isDriveChange = true;
        Ext.ComponentQuery.query('#editor-output')[0].loadSettings(JSON.parse(settingsStr));
        this._isDriveChange = false;
    },
    onDriveCodeChange: function (code) {
        this._isDriveChange = true;
        Ext.ComponentQuery.query('#editor-output')[0].setCode(code);
        this._isDriveChange = false;
    },
    onDriveObjectChange: function (evt) {
        // only handle change if the change is remote or if this is a history action
        if (!evt.isLocal) {
          this.loadModelData();
        }
    },

    onDataEditorValueChange: function () {
        if (this._isHistoryAction || this._isDriveChange) return;

        var model = this._model;
        var root = this._root;
        var store = Datamatic.getTreeDataStore();

        model.beginCompoundOperation();
        var dataObject = root.get("data");
        var stringData = JSON.stringify(store.getRootData());
        dataObject.setText(stringData);
        model.endCompoundOperation();
    },

    onSettingsChange: function () {
        if (this._isHistoryAction || this._isDriveChange) return;

        var model = this._model;
        var root = this._root;
        var settings = Ext.ComponentQuery.query('#editor-output')[0].getSettings();

        model.beginCompoundOperation();
        var settingsObject = root.get("settings");
        settingsObject.setText(JSON.stringify(settings));
        model.endCompoundOperation();
    },

    onCodeChange: function (code) {
        if (this._isHistoryAction || this._isDriveChange) return;

        var model = this._model;
        var root = this._root;

        model.beginCompoundOperation();
        var codeObject = root.get("code");
        codeObject.setText(code);
        model.endCompoundOperation();
    },

    onSelectionChange: function (sm, selected) {
        var selectedNode = selected[0];
        if (selectedNode) {
            this.saveSelectionPath(selectedNode);
        }
    },

    saveSelectionPath: function (selectedNode) {
        this._selectionPath = selectedNode.getPath('index');
    },

    redo: function () {
        this._isHistoryAction = true;
        this._model.redo();
        this.loadModelData();
        this._isHistoryAction = false;
    },

    undo: function () {
        this._isHistoryAction = true;
        this._model.undo();
        this.loadModelData();
        this._isHistoryAction = false;
    },

    loadModelData: function(){
      var root = this._model.getRoot();
      this.onDriveSettingsChange(root.get("settings").getText());
      this.onDriveDataChange(root.get("data").getText());
      this.onDriveCodeChange(root.get("code").getText());
    },

    closeDocument: function () {
        var output = Ext.ComponentQuery.query('#editor-output')[0];
        var treeDataStore = Datamatic.getTreeDataStore();
        var dataSelectionModel = Datamatic.drive.getDataTree().getSelectionModel();

        // save data to the document on change
        treeDataStore.un("change", this.onTreeDataStoreChange, this);
        output.un("change", this.onEditorOutputChange, this);
        output.un("codeChange", this.onCodeChange, this);
        dataSelectionModel.un("selectionchange", this.onSelectionChange, this);

        Ext.fly("google-publish-btn").hide();

        if (this._doc) {
            this._doc.close();
            delete Datamatic.drive._doc;
        }

        if (this._model) {
            this.fireEvent("documentClosed");
            delete this._model;
        }
    },

    onTreeDataStoreChange: function () {
        if (!this.isHistoryAction() && !this.isDriveChange()) {
            this.dataEditorValueChangeTask.delay(250);
        }
    },

    onEditorOutputChange: function () {
        if (!this.isHistoryAction() && !this.isDriveChange()) {
            this.settingsChangeTask.delay(250);
        }
    },

    onLaunch: function () {
        this.signInWindow = new Datamatic.view.googledrive.SignInWindow();
        this._realtimeOptions = new Datamatic.controller.googledrive.RealtimeOptions();
        var realtimeLoader = this.realtimeLoader = new rtclient.RealtimeLoader(this._realtimeOptions);

        this.renameWindow = new Datamatic.view.googledrive.RenameWindow();
        this.publishWindow = new Datamatic.view.googledrive.PublishWindow();
        this.publishSettingsWindow = new Datamatic.view.googledrive.PublishSettingsWindow();
        this.copyWindow = new Datamatic.view.googledrive.CopyWindow();

        Ext.get("datamatic-file-title").on("click", this.renameFile, this);

        gapi.load('picker');

        Datamatic.openFile = function () {
            this.openFile.apply(this, arguments);
        }.bind(this);

        Datamatic.drive = this;

        this.realtimeLoader.start();

        Datamatic.retrieveAllFiles = function () {
            this.retrieveAllFiles.apply(this, arguments);
        }.bind(this);

        Ext.get(document).on("keydown", function (ev) {
            //CTRL + Z
            if (ev.ctrlKey && ev.keyCode == 90 && this._model && this._model.canUndo) {
                this.undo();
            }

            //CTRL + Y
            if (ev.ctrlKey && ev.keyCode == 89 && this._model && this._model.canRedo) {
                this.redo();
            }
        }, this);

        this._signOutEl = Ext.getBody().createChild({
            tag: "iframe",
            style: "display:none"
        })

        this.dataEditorValueChangeTask = new Ext.util.DelayedTask(function () {
            Datamatic.drive.onDataEditorValueChange();
        });

        this.settingsChangeTask = new Ext.util.DelayedTask(function () {
            Datamatic.drive.onSettingsChange();
        });


    },

    signOut: function () {
        Datamatic.drive.authDetails = null;
        Ext.fly("google-user-info").update('').hide();
        Ext.fly("google-sign-in-btn").show().on("click", this.authorizeWithPopup, this);
        Ext.fly("google-sign-out-btn").hide();
        Ext.fly("google-publish-btn").hide();
        Ext.ComponentQuery.query('#file-menu')[0].disable();
        this.closeDocument();
        gapi.auth.signOut();
    },

    getNewDocumentData: function(){
        return this._newDocumentData;
    },

    createNewFileAndRedirect: function (newDocumentData, callback) {
        this.showLoader();
        Ext.get("datamatic-file-title").update(DATAMATIC_DEFAULT_TITLE);
        Ext.fly("google-publish-btn").hide();
        this._newDocumentData = newDocumentData;
        this.realtimeLoader.createNewFileAndRedirect(callback);
    },

    copyFile: function () {
        if (this._fileDetails) {
            this.copyWindow.show(Ext.get("datamatic-file-title").dom.innerHTML);
        }
    },

    doCopy: function (newTitle, success, failure) {
        Datamatic.drive.showLoader();

        Ext.get("datamatic-file-title").update(newTitle);

        this.realtimeLoader.createNewRealtimeFile(newTitle,
        // initialize model - copy current file
        function(model){
          var root = model.getRoot();
          var treeDataStore = Datamatic.getTreeDataStore();
          var output = Ext.ComponentQuery.query('#editor-output')[0];

          // TODO: refactor this using file object
          root.set("data", model.createString(JSON.stringify(treeDataStore.getRootData())));
          root.set("settings", model.createString(JSON.stringify(output.getSettings())));
          //root.set("template", model.createString(templatesSelectionModel.getLastSelected().data.id));
          root.set("code", model.createString(output.getCode()));
          root.set("template", model.createString(output.getTemplateID()));
        },
        // handle success
        function (fileIds, userID) {
          // close current document before loading new data
            this.closeDocument();
            success();
            this.realtimeLoader.redirectTo(fileIds, userID, false);
        }.bind(this), function(message){
          alert(message);
          Datamatic.drive.hideLoader();
          Ext.get("datamatic-file-title").update(this.getFileTitle());
        }.bind(this));
    },

    renameFile: function () {
        if (!this.isAuthenticated()) {
            this.authorizeWithPopup();
        } else if (this.isFileOpened()) {
            this.renameWindow.show(Ext.get("datamatic-file-title").dom.innerHTML);
        }
    },

    doRename: function (newTitle, success) {
        var fileIds = rtclient.params['fileIds'].split(",");
        Ext.get("datamatic-file-title").update(newTitle);
        this._fileDetails.title = newTitle;

        if (fileIds[0]) {
            gapi.client.load('drive', 'v2', function () {
                var data = {
                    'title': newTitle
                };
                var request = gapi.client.drive.files.patch({
                    'fileId': fileIds[0],
                    'resource': data
                });
                request.execute(function (resp) {
                    if (success) success();
                    Ext.get("datamatic-file-title").update(resp.title);
                });
            });
        } else {
            if (failure) failure("No file is currenlty opened.");
        }
    },

    getRealtimeLoader: function () {
        return this.realtimeLoader;
    },

    openFile: function () {
        var token = gapi.auth.getToken().access_token;
        var docsView = new google.picker.DocsView(google.picker.ViewId.DOCS);

        docsView.setIncludeFolders(true);
        docsView.setMode(google.picker.DocsViewMode.LIST);
        docsView.setMimeTypes("application/vnd.google-apps.drive-sdk." + this._realtimeOptions.appId);
        docsView.setOwnedByMe(true);

        var sharedView = new google.picker.DocsView(google.picker.ViewId.DOCS);

        sharedView.setIncludeFolders(true);
        sharedView.setMode(google.picker.DocsViewMode.LIST);
        sharedView.setMimeTypes("application/vnd.google-apps.drive-sdk." + this._realtimeOptions.appId);
        sharedView.setOwnedByMe(false)

        var recentView = new google.picker.DocsView(google.picker.ViewId.RECENTLY_PICKED);

        recentView.setIncludeFolders(true);
        recentView.setMode(google.picker.DocsViewMode.LIST);
        recentView.setMimeTypes("application/vnd.google-apps.drive-sdk." + this._realtimeOptions.appId);

        var picker = new google.picker.PickerBuilder()
            .setAppId(this._realtimeOptions.appId)
            .setOAuthToken(token)
            .addView(docsView)
            .addView(sharedView)
            .addView(recentView)
            .setCallback(this._realtimeOptions.openCallback.bind(this))
            .build();
        picker.setVisible(true);
    },

    /* Retrieve a list of File resources.
     *
     * @param {Function} callback Function to call when the request is complete.
     */
    retrieveAllFiles: function (callback) {
        var retrievePageOfFiles = function (request, result) {
            request.execute(function (resp) {
                result = result.concat(resp.items);
                callback(result);
            });
        }
        var initialRequest = gapi.client.drive.files.list({
            "q": "mimeType= 'application/vnd.google-apps.drive-sdk."+Ext.manifest.vars.GOOGLE_APP_ID+"' and trashed = false"
        });
        retrievePageOfFiles(initialRequest, []);
    },

    getFileDetails: function () {
        return this._fileDetails;
    },

    getFileTitle: function () {
        return this._fileDetails ? this._fileDetails.title : this._forkTitle || DATAMATIC_DEFAULT_TITLE;
    },

    isFileOpened: function () {
        return rtclient.params['fileIds'] || (rtclient.params['state'] && rtclient.params['state'].indexOf('state')==1);
    },

    isForkRequested: function(){
        return !!rtclient.params['forkId'];
    },

    isForkLoaded: function(){
      return this._forkLoaded;
    },

    isAuthenticated: function () {
        return !!Datamatic.drive.authDetails;
    },

    showLoader: function () {
        Ext.ComponentQuery.query('#main-viewport')[0].addCls("datamatic-loading");
    },

    hideLoader: function () {
        Ext.ComponentQuery.query('#main-viewport')[0].removeCls("datamatic-loading");
    },

    // we need to load old .name and .size properties as A/B
    // TODO: remove this when unused
    handleLegacyDataLoad: function(data){
        var fixChildren = function(children){
            if (!children) return;
            children.forEach(function(child){
                if (child.A == null && child.name != null){
                    child.A = child.name;
                }

                if (child.B == null && child.size != null){
                    child.B = child.size;
                }

                fixChildren(child.children);
            });
        }

        fixChildren(data.children);
    }
});
