var DATAMATIC_DEFAULT_TITLE = Ext.manifest.vars.DATAMATIC_DEFAULT_TITLE;

Ext.define('Datamatic.controller.googledrive.RealtimeOptions', {

    /**
     * Client ID from the console.
     */
    clientId: Ext.manifest.vars.GOOGLE_CLIENT_ID,

    /**
     * The ID of the button to click to authorize. Must be a DOM element ID.
     */
    authButtonElementId: 'google-sign-in-btn',

    appId: Ext.manifest.vars.GOOGLE_APP_ID,
    /**
     * The MIME type of newly created Drive Files. By default the application
     * specific MIME type will be used:
     *     application/vnd.google-apps.drive-sdk.
     */
    newFileMimeType: null,

    /**
     * Autocreate files right after auth automatically.
     */
    autoCreate: false,


    /**
     * The name of newly created Drive files.
     */
    defaultTitle: DATAMATIC_DEFAULT_TITLE,

    /**
     * Function to be called every time a Realtime file is loaded.
     */
    onFileLoaded: function (doc) {
        var model = doc.getModel();
        var root = model.getRoot();
        var output = Ext.ComponentQuery.query('#editor-output')[0];
        var treeDataStore = Datamatic.getTreeDataStore();
        var dataSelectionModel = Datamatic.drive.getDataTree().getSelectionModel();

        Datamatic.drive._doc = doc;
        Datamatic.drive._model = model;
        Datamatic.drive._root = root;

        var templateID = root.get("template").getText();
        output.setTemplate(root.get("template").getText(), true).then(function(){
            var treeData = JSON.parse(root.get("data").getText());
            Datamatic.drive.handleLegacyDataLoad(treeData);
            Datamatic.getTreeDataStore().setRootNode(treeData);

            var code = root.get("code").getText();
            output.setCode(code);
            output.loadSettings(JSON.parse(root.get("settings").getText()));

            // bind document history
            model.addEventListener(gapi.drive.realtime.EventType.UNDO_REDO_STATE_CHANGED, Datamatic.drive.onUndoRedoStateChanged.bind(Datamatic.drive));

            // bind onchange event
            root.addEventListener(gapi.drive.realtime.EventType.OBJECT_CHANGED, Datamatic.drive.onDriveObjectChange.bind(Datamatic.drive));

            Datamatic.drive.hideLoader();

            // save data to the document on change
            treeDataStore.on("change", Datamatic.drive.onTreeDataStoreChange, Datamatic.drive);
            output.on("change", Datamatic.drive.onEditorOutputChange, Datamatic.drive);
            output.on("codeChange", Datamatic.drive.onCodeChange, Datamatic.drive);
            dataSelectionModel.on("selectionchange", Datamatic.drive.onSelectionChange, Datamatic.drive);
            dataSelectionModel.selectRange(1, 1);


            // update file title & handle rename
            gapi.client.load('drive', 'v2', function () {
                var fileIds = rtclient.params['fileIds'];

                if (!rtclient.share) {
                    rtclient.share = new gapi.drive.share.ShareClient(Ext.manifest.vars.GOOGLE_APP_ID);
                }
                rtclient.share.setItemIds(fileIds);

                Ext.fly("google-publish-btn").show();

                rtclient.getFileMetadata(fileIds.split(',')[0], function (resp) {
                    Ext.get("datamatic-file-title").update(resp.title);
                    Datamatic.drive._fileDetails = resp;
                });
            });
        }.bind(this));
    },
    initializeModel: function (model) {
        var root = model.getRoot();

        var newDocumentData = Datamatic.drive.getNewDocumentData();

        if (Datamatic.drive.forkId){
            root.set("parentForkId", Datamatic.drive.forkId);
            delete Datamatic.drive.forkId;
        }

        root.set("data", model.createString(JSON.stringify(newDocumentData.getData())));
        root.set("settings", model.createString(JSON.stringify(newDocumentData.getSettings())));
        root.set("template", model.createString(newDocumentData.getTemplateID()));
        root.set("code", model.createString(newDocumentData.getCode()));

        model.addEventListener(gapi.drive.realtime.EventType.UNDO_REDO_STATE_CHANGED, Datamatic.drive.onUndoRedoStateChanged.bind(Datamatic.drive));            
    },
    openCallback: function (data) {
        if (data.action == google.picker.Action.PICKED) {
            Datamatic.drive.closeDocument();
            var fileId = data.docs[0].id;
            this.realtimeLoader.redirectTo([fileId], this.realtimeLoader.authorizer.userId);
            Datamatic.drive.showLoader();
        } else if (data.action == google.picker.Action.CANCEL) {
            if (!Datamatic.drive.isFileOpened()) {
                Datamatic.drive.openFile();
            }
        }
    },

    /**
     * Function to be called to inityalize custom Collaborative Objects types.
     */
    registerTypes: null, // No action.

    /**
     * Function to be called after authorization and before loading files.
     */
    afterAuth: function (authDetails) {
        Datamatic.drive.getUserInfo(authDetails).then(function(){
            Datamatic.drive.authDetails = authDetails;
            Datamatic.drive.fireEvent("afterAuth", authDetails);
            Datamatic.drive.signInWindow.hide();

            Ext.fly("google-user-info").update(authDetails.name).show();
            Ext.ComponentQuery.query('#file-menu')[0].enable();
            Ext.fly("google-sign-out-btn").show();

            // if no files are given and authorized on load
            if (Datamatic.drive.isForkRequested() && !Datamatic.drive.isForkLoaded()){
                Datamatic.drive.loadForkData();
            } else if (!Datamatic.drive.isFileOpened()) {
                Datamatic.drive.openFile();
            }

            if (authDetails.email) {
                ga('send', 'event', 'sign-in', authDetails.email, authDetails.given_name+" "+authDetails.family_name+"("+authDetails.name+")");

                UserVoice.push(['identify', {
                    email: authDetails.email, // User’s email address
                    name: authDetails.name // User’s real name
                }]);
            }
        }, this);
    },
    onAuthFailed: function (details) {
        Datamatic.drive.authDetails = null;
        Datamatic.drive.onloadAuthFailed = true;
        Datamatic.drive.fireEvent("onAuthFailed");

        Ext.get("datamatic-file-title").update(DATAMATIC_DEFAULT_TITLE);
        Datamatic.drive.signInWindow.show();
    }
});
