Ext.define('Datamatic.controller.MainToolbarController', {
    extend: 'Ext.app.Controller',
    mixins: {
        history: 'Datamatic.controller.HistoryControllerMixin'
    },
    refs: [
       {
           ref: 'editor',
           selector: '#data-editor'
       },
       {
           ref: 'undoButton',
           selector: '#main-toolbar-undo-btn'
       },
       {
           ref: 'redoButton',
           selector: '#main-toolbar-redo-btn'
       }
    ],
    onLaunch: function(){
    	this.mixins.history.onLaunch.apply(this, arguments);
    }
});
