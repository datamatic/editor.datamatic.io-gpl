Ext.define('Datamatic.controller.UserPlanRestrictionsController', {
    extend: 'Ext.app.Controller',
    onLaunch: function(){
        this._themeNames = [];
        this._properties = [];
        Datamatic.drive.on("afterAuth", function(authDetails){
        Datamatic.drive.getUserInfo(authDetails)
          .then(this._restrictForkData.bind(this))
          .then(this._restrictWatermark.bind(this))
          .then(this._refreshSettings.bind(this));
      }, this);
    },
    _restrictForkData: function(userInfo){
        Datamatic.drive.on("b4loadForkData", function(data){
            var isFreePlan =  userInfo.plan == "Free";

            var settings = data.getSettings();
            if (isFreePlan) {
                settings["watermark-type"] = "datamatic";
            }
        }, this);

        var df = $.Deferred();
        df.resolve(userInfo);
        return df.promise();
    },
    _restrictWatermark: function(userInfo){
      var plan = userInfo.plan;
      this._properties.push({
            folder:"Watermark",
            property: "watermark-url",
            noPropertyEditor:true,
            value:Ext.manifest.vars["WATERMARK_URL"]
       });

      if (plan != "Free") {
        this._properties.push({
             folder: "Watermark",
             property: "watermark-type",
             values:["none", "datamatic", "background"],
             defaultValue: "none"
         });
     } else {
         this._properties.unshift({
              folder: "Watermark",
              property: "watermark-type",
              values:["datamatic"],
              defaultValue: "datamatic"
          });
     }

     var df = $.Deferred();
     df.resolve(userInfo);
     return df.promise();
    },
    _refreshSettings: function(){
      Ext.ComponentQuery.query('#editor-output')[0].refreshSettings();
    },
    getProperties: function(){
        return this._properties;
    }
});
