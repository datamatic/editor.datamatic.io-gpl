Ext.define('Datamatic.controller.DataEditorController', {
    extend: 'Ext.app.Controller',
    refs: [{
        ref: 'editor',
        selector: '#data-editor'
    }, {
        ref: 'output',
        selector: '#editor-output'
    },{
        ref: 'breadcrumb',
        selector: '#data-editor-breadcrumb'
    }],
    onLaunch: function () {
        // get tree selection model reference and listen to selectionchange event
        this._selectionModel = this.application.getTreeSelectionModel();
        this._selectionModel.on("selectionchange", this._onSelectionChange, this);

        // listen to editor events
        var editor = this.getEditor();
        editor.on("afterSelection", this._afterSelection, this);
        editor.on("afterRemoveRow", this._afterRemoveRow, this);
        editor.on("afterRemoveCol", this._afterRemoveCol, this);
        editor.on("removeSelectedRows", this.removeSelectedRows, this);
        editor.on("afterEditorChange", this._afterEditorChange, this);
        editor.on("afterCreateRow", this._afterCreateRow, this);
        editor.on("navigateUp", this._onNavigateUp, this);
        editor.on("navigateDown", this._onNavigateDown, this);
        editor.on("copy", this._onCopy, this);
        editor.on("paste", this._onPaste, this);
        editor.on("cut", this._onCut, this);

        var breadcrumb = this.getBreadcrumb();
        breadcrumb.on("navigateUp", this._onNavigateUp, this);
        breadcrumb.on("navigateDown", this._onNavigateDown, this);
        breadcrumb.on("navigateTo", this._onNavigateTo, this);
    },
    _getNodeData: function(nodeData){
        var data = [];
        var i = 0;
        var fieldName = Handsontable.helper.spreadsheetColumnLabel(i);

        while (nodeData[fieldName] != null){
            data.push(nodeData[fieldName]);
            i++;
            fieldName = Handsontable.helper.spreadsheetColumnLabel(i);
        }

        return data;
    },
    _nodeChildrenToArray: function(children){
        var data = [];
        for (var i = 0; i < children.length; i++) {
            var item = children[i];
            var itemData = this._getNodeData(item);
            data.push(itemData);
        }

        this._normalizeData(data);

        return data;
    },
    // trims empty values at the end of the row, always leaves one column even if empty
    _trimRow: function(row){
        var trimmedRow = row.slice(0);
        for (var i = row.length-1; i>0; i--) {
            if (row[i] == "" || row[i] == null) {
                trimmedRow.length = i;
            } else return trimmedRow;
        }

        return trimmedRow;
    },
    // trims empty rows, always leaves one row, even if empty
    _trimRows: function(rows){
        for (var i = rows.length-1; i>0 ; i--) {
            var row = rows[i];
            for (var j = 0; j < row.length; j++) {
                if (row[j] !== "" && row[j] != null) return rows;
            }
            // if none of the rows are empty, remove this row
            rows.length = i;
        }

        return rows;
    },
    _normalizeData: function(data){
        var lengths = data.map(function(row){
            var trimmedRow = this._trimRow(row);
            return trimmedRow.length;
        }.bind(this));

        var maxLength = Math.max.apply(Math, lengths);

        data.forEach(function(row){
            row.length = maxLength;
        });

        data = this._trimRows(data);

        return data;
    },
    _onSelectionChange: function (sm, selected) {
        var nothingSelected = !this._selectedNode;
        var selectedNode = this._selectedNode = selected[0];

        var breadcrumb = this.getBreadcrumb();
        breadcrumb.update(selectedNode);

        // do nothing when there is no selection
        if (!selectedNode || this._localSelect) return;

        var editor = this.getEditor();
        var parentNode = this._parentNode = selectedNode.parentNode;

        // set editor data
        var data = this._nodeChildrenToArray(parentNode.data.children);

        // prevent creating new rows
        this._isLocal = true;
        editor.setData(data, nothingSelected);
        this._isLocal = false;
        // select row
        editor.selectRow(parentNode.indexOf(selectedNode));
    },

    removeSelectedRows: function (selected) {
        // prevent deleting root
        if (!this._parentNode || this._parentNode.isRoot()) return;

        var topRow = selected.getTopLeftCorner().row;
        var bottomRow = selected.getBottomRightCorner().row;
        var amount = bottomRow - topRow + 1;

        if (amount){
            // prevent auto creating row when deleting rows
            this._isLocal = true;
            this.getEditor().removeRows(topRow, amount);
            this._isLocal = false;
        }
    },

    _afterRemoveRow: function (rowIndex, amount) {
        var childNode = this._parentNode.getChildAt(rowIndex);

        while (childNode && amount) {
            this._parentNode.removeChild(childNode);

            //not sure why the store doesn't update this automatically, might need proxy to be defined
            this._parentNode.data.children.splice(rowIndex, 1);

            this._selectedNode = null;
            amount--;
            childNode = this._parentNode.getChildAt(rowIndex);
        }

        //select parent node if no other children are available
        if (!this._parentNode.data.children.length) {
            setTimeout(function () {
              if (this._isAllowedNode(this._parentNode)) {
                this._selectionModel.select(this._parentNode);
              }
            }.bind(this));
        }

        this.getBreadcrumb().update(null, this._parentNode.lastChild);
    },
    // TODO: handle multiple rows, although we don't support this in the UI atm
    _afterCreateRow: function(rowIndex, amount){
        // prevent auto creating row when deleting rows
        if (this._isLocal) return;
        if (!this._parentNode) return;

        var nodeData = {};
        nodeData["leaf"] = true;
        nodeData["A"] = "";

        var newNode = Ext.create('Datamatic.model.TreeDataModel', nodeData);
        this._parentNode.insertChild(rowIndex, newNode);

        // not sure why the store doesn't update this automatically, might need proxy to be defined
        this._parentNode.data.children.splice(rowIndex, 0, newNode.data);

        this.getBreadcrumb().update(null, newNode);
        Datamatic.drive.saveSelectionPath(newNode);
    },
    _afterRemoveCol: function(colIndex, amount){
        if (!this._parentNode) return;

        this._parentNode.childNodes.forEach(function(node, index){
            var field = Handsontable.helper.spreadsheetColumnLabel(index);
            node.set(field, null, {
                commit: true
            });
            delete node.data[field];
        });
    },
    _ensureEmptyPrevColumns: function(col, node){
        var i = col-1;
        while (i >=0){
            var fieldName = Handsontable.helper.spreadsheetColumnLabel(i);
            if (typeof node.get(fieldName) == "undefined"){
                node.set(fieldName, "");
            }

            i--;
        }
    },
    _afterEditorChange: function (change, type) {
        var row = change[0];
        var newValue = change[3];
        var col = change[1];
        var field = Handsontable.helper.spreadsheetColumnLabel(col);
        var editor = this.getEditor();

        if (!this._parentNode) return;

        var node = this._parentNode.getChildAt(row);

        // change
        if (node) {
            node.set(field, newValue, {
                commit: true
            });

            this._ensureEmptyPrevColumns(col, node);
            this.getBreadcrumb().update(this._selectedNode);
            // new
        } else {
            var nodeData = {};
            nodeData[field] = newValue;
            nodeData["leaf"] = true;

            node = Ext.create('Datamatic.model.TreeDataModel', nodeData);

            this._parentNode.appendChild(node, false, true);

            // not sure why the store doesn't update this automatically, might need proxy to be defined
            this._parentNode.data.children.push(node.data);

            this._parentNode.store.add(node);

            //this.getBreadcrumb().update(null, newNode);
            Datamatic.drive.saveSelectionPath(node);
        }

        // if (type == "paste" && col === 0) {
        //     this._handleDeepPaste(this._clipBoard, node);
        // }
    },

    _onCopy: function(){
        this._clipBoard = this._selectedNode;
    },

    _onCut: function(){
        this._clipBoard = this._selectedNode;
    },

    _onPaste: function(isShiftKey){
        // disable deep copy if SHIFT is pressed
        if (isShiftKey){
            this._clipBoard = null;
        }
    },

    _removeChildNodes: function(node){
        node.childNodes.forEach(function(child){
            node.removeChild(child);
        });
    },

    _copyNode: function(fromNode, intoNode){
        if (!fromNode.childNodes) return;

        intoNode.data.children = [];
        fromNode.childNodes.forEach(function(childNode){
            var clone = childNode.copy();
            intoNode.appendChild(clone, false, true);
            intoNode.data.children.push(clone.data);

            this._copyNode(childNode, clone);
        }.bind(this));
    },
    _handleDeepPaste: function(fromNode, intoNode){
        if (fromNode && intoNode){
            this._removeChildNodes(intoNode);

            this._isLocal = true;
            this._copyNode(fromNode, intoNode);
            this._isLocal = false;

            this._selectionModel.select(intoNode);
        }
    },

    _afterSelection: function (rowIndexStart, cellIndexStart, rowEndIndex, cellEndIndex) {
        this._localSelect = true;
        var selectedNode = this._parentNode.getChildAt(rowIndexStart);
        if (selectedNode) {
            this._selectionModel.select(this._parentNode.getChildAt(rowIndexStart));
        } else {
            this._selectionModel.deselectAll();
        }

        this._localSelect = false;
    },

    _onNavigateTo: function (node) {
        this._selectionModel.select(node);
    },

    _isAllowedNode: function(node){
      return !node.isRoot() && node.parentNode && !node.parentNode.isRoot();
    },

    _onNavigateUp: function () {
        var parentNode = this._parentNode;

        if (this._isAllowedNode(this._parentNode)) {
            this._selectionModel.select(this._parentNode);
        }
    },
    _onNavigateDown: function () {
        var selectedData = this.getEditor().getSelectedData()[0][0];
        var selectedNode = this._selectedNode;
        if (selectedData == null || !selectedNode) return;
        if (!this._isAllowedNode(selectedNode)) return;

        if (selectedNode.firstChild) {
            this._selectionModel.select(selectedNode.firstChild);
        } else {
            var newNode = Ext.create('Datamatic.model.TreeDataModel', {
                A: "",
                leaf: true
            });

            selectedNode.appendChild(newNode, false, true);

            // not sure why the store doesn't update this automatically, might need proxy to be defined
            selectedNode.data.children = [newNode.data];
            selectedNode.store.add(newNode);

            //FIXME:
            // selecting twice as the first call is prevented due to mechanism in the data editor
            // this needs to be fixed when refactoring selection model
            this._selectionModel.select(newNode);
        }
    }
});
