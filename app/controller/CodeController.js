Ext.define('Datamatic.controller.CodeController', {
    extend: 'Ext.app.Controller',
    refs: [{
        ref: 'editor',
        selector: '#code-view'
    }, {
        ref: 'output',
        selector: '#editor-output'
    },
    {
        ref:'publishSettingsMenuItem',
        selector:'#publish-settings-menu-item'
    },
    {
        ref: 'toolbarButton',
        selector: '#main-toolbar-code-btn'
    }],
    _isDeveloper: function(userInfo){
        return userInfo.roles && userInfo.roles.indexOf("developer") != -1;
    },
    onLaunch: function () {
        var editor = this.getEditor();
        var output = this.getOutput();
        var savedCode;

        Datamatic.drive.on("afterAuth", function(authDetails){
          Datamatic.drive.getUserInfo(authDetails).then(function(userInfo){
            if (this._isDeveloper(userInfo)){
              this.getToolbarButton().show();
              this.getEditor().show();
              this.getPublishSettingsMenuItem().show();
            }
          }.bind(this));
        }, this)

        output.on("templateChange", function (templateID) {
              savedCode = output.getCode();
              this._local = true;
              editor.setCode(savedCode);
              this._local = false;
              this.getToolbarButton().setText("Code");
        }, this);

        output.on("codeChange", function (code) {
            if (!this._local){
              savedCode = code;
              editor.setCode(savedCode);
              this.getToolbarButton().setText("Code");
            }
        }, this);

        editor.on("save", function (newCode) {
            savedCode = newCode;
            this.getToolbarButton().setText("Code");
            this._local = true;
            output.setCode(savedCode).then(function(){
                this._local = false;
            }.bind(this));
        }, this);

        editor.on("change", function (changedCode) {
            if (changedCode != savedCode) {
                this.getToolbarButton().setText("Code*");
            } else {
                this.getToolbarButton().setText("Code");
            }
        }.bind(this));
    }
});
