/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('Datamatic.Application', {
    extend: 'Ext.app.Application',
    requires: ['Datamatic.document.DocumentManager', 'Datamatic.document.NewDocumentData'],
    name: 'Datamatic',
    models: ['TreeDataModel'],
    stores: ['TreeData'],
    views: [
        'tools.Properties',
        'tools.Data',
        'tools.DataEditor',
        'tools.DataTree',
        'tools.Result'
    ],

    controllers: [
        'MainViewportController',
        'DataEditorController',
        'MainToolbarController',
        'MainMenuController',
        'googledrive.GoogleDriveSupport',
        'CodeController',
        'PublishController',
        'UserPlanRestrictionsController'
    ],

    launch: function(){
    	// disable loader on launch, TODO: uses classes for this
    	Ext.getBody().setStyle("visibility", "visible").parent().setStyle("background","");

    	// setting references to Datamatic, TODO: refactor this using proper references
        Datamatic.getTreeDataStore = this.getTreeDataStore.bind(this);
        Datamatic.getTreeSelectionModel = this.getTreeSelectionModel;
        Datamatic.getOutput = function(){
            return Ext.ComponentQuery.query('#editor-output')[0];
        }

        Datamatic.documentMgr = new Datamatic.document.DocumentManager();
    },

    getUserPlanRestricionsController: function(){
        return this.getUserPlanRestricionsController();
    },

    getTreeSelectionModel: function(){
    	// TODO: replace this by a decoupled selection model
    	return Ext.ComponentQuery.query('#data-tree')[0].getSelectionModel();
    }
});

Ext.Loader.setConfig({
    disableCaching: false
});

// FIX Firefox 52 click issue
if (Ext.isIE || Ext.isEdge || (Ext.firefoxVersion >= 52) || Ext.os.is.ChromeOS || window.inUnitTest) {
    var eventMap = Ext.dom.Element.prototype.eventMap;
    eventMap.click = 'click';
    eventMap.dblclick = 'dblclick';
    eventMap.touchstart = 'mousedown';
    eventMap.touchmove = 'mousemove';
    eventMap.touchend = 'mouseup';
    eventMap.touchcancel = 'mouseup';

    eventMap.mousedown = 'mousedown';
    eventMap.mousemove = 'mousemove';
    eventMap.mouseup = 'mouseup';
    eventMap.touchstart = 'touchstart';
    eventMap.touchmove = 'touchmove';
    eventMap.touchend = 'touchend';
    eventMap.touchcancel = 'touchcancel';

    eventMap.pointerdown = 'mousedown';
    eventMap.pointermove = 'mousemove';
    eventMap.pointerup = 'mouseup';
    eventMap.pointercancel = 'mouseup';
}